var searchModule =  angular.module('kartSearchModule');

searchModule.controller('KartSearchCtrl',['$scope','$rootScope','KartSearchService','$location',  function($scope, $rootScope, KartSearchService,$location){
	$scope.metadataList=[];
	//$scope.vm.resultList =[];
	//Advanced Search
	$scope.doSearch= function(){
		$scope.vm.resultList =[];
		var itemSearchObj ={};
		if($scope.searchText != null){
			itemSearchObj = {itemDescription: $scope.searchText};
			
		}
		var itemSearch = angular.toJson(itemSearchObj);
		//alert('itemSearch'+itemSearch);
		KartSearchService.searchByCriteria(itemSearch, function (response) {
			if (response.success) {
				$rootScope.detailsSelection= 'onSimpleSearch';
				$scope.vm.resultList = response.data;
	        	 //MerchantInfoService.updateInfo(null, null);
	        	 $rootScope.merchantShopName = '';
			 } else {
			    $scope.vm.errorMessages = [];
			     $scope.vm.errorMessages.push({description: response.message});
			     $scope.test=response.message;
			     //MerchantInfoService.updateInfo(null, null);
			     $rootScope.merchantShopName = '';
			 }
		});
	};
	//Advanced Search
	$scope.advancedSearch= function(){
		//$scope.vm.resultList =[];
		//alert($scope.selectionCat);
		var selectedCategories = $scope.vm.selectionCat;
		var selectedMetalTypes = $scope.vm.selectionMet;
		var selectedCountyCd = $scope.vm.shopCountry;
		var selectedStateCd = $scope.vm.shopState;
		var selectedCity = $scope.vm.city;
		var itemSearchObj ={};
		if(selectedCategories.length >0){
			itemSearchObj.categories = selectedCategories;
		}
		if(selectedMetalTypes.length >0){
			itemSearchObj.metalTypes = selectedMetalTypes;
		}
		if(selectedCountyCd != null){
			itemSearchObj.countryCode = selectedCountyCd;
		}
		if(selectedStateCd != null){
			itemSearchObj.stateCode = selectedStateCd;
		}
		if(selectedCity != null){
			itemSearchObj.city = selectedCity;
		}
		var itemSearch = angular.toJson(itemSearchObj);
		//alert('itemSearch'+itemSearch);
		KartSearchService.searchByCriteria(itemSearch, function (response) {
			if (response.success) {
				$rootScope.detailsSelection= 'onAdvancedSearch';
				$scope.vm.resultList = response.data;
	        	 //MerchantInfoService.updateInfo(null, null);
	        	 $rootScope.merchantShopName = '';
			 } else {
			    $scope.vm.errorMessages = [];
			     $scope.vm.errorMessages.push({description: response.message});
			     $scope.test=response.message;
			     //MerchantInfoService.updateInfo(null, null);
			     $rootScope.merchantShopName = '';
			 }
		});
	};
	
	//Top Trending items list
	$scope.getTopTrendingItems = function() {
		$rootScope.detailsSelection= 'onTopTrending';
		KartSearchService.findTopTrendingItems(function(response){
            if (response.success) {
            	$scope.metadataList = response.data;
            	$scope.slides = response.data;
            	 //MerchantInfoService.updateInfo(null, null);
            	 $rootScope.merchantShopName = '';
			 } else {
			    $scope.vm.errorMessages = [];
			     $scope.vm.errorMessages.push({description: response.message});
			     $scope.test=response.message;
			     //MerchantInfoService.updateInfo(null, null);
			     $rootScope.merchantShopName = '';
			 }
		}); 
	};
	$scope.myInterval = -1;
	  $scope.slides = [];
	  
	  $scope.$watch('slides', function(values) {

	    var i, a = [], b;

	    for (i = 0; i < $scope.slides.length; i += 3) {
	      b = { item1: $scope.slides[i] };

	      if ($scope.slides[i + 1]) {
	        b.item2 = $scope.slides[i + 1];
	      }
	      if ($scope.slides[i + 2]) {
		        b.item3 = $scope.slides[i + 2];
		      }
	      if ($scope.slides[i + 3]) {
		        b.item4 = $scope.slides[i + 3];
		      }
	      a.push(b);
	    }

	    $scope.groupedSlides = a;

	  }, true);
	  
	  $scope.goBack = function(){
			 //alert('GoBack:'+$rootScope.detailsSelection);
		  	if($rootScope.merchantShopName != null || $rootScope.merchantShopName == ''){
		  		 //window.location.replace('#/'+$rootScope.merchantShopName);
		  		 $location.path('/'+$rootScope.merchantShopName);
		  	}else{
			 if($rootScope.detailsSelection != null){
				 if($rootScope.detailsSelection === 'onTopTrending'){
					// window.location.replace('#/');
					 location.path('/');
				 } else if($rootScope.detailsSelection === 'onSimpleSearch'){
					 //window.location.replace('#/onSimpleSearch');
					 location.path('/onSimpleSearch');
				 } 
				 else if($rootScope.detailsSelection === 'onAdvancedSearch'){
					// window.location.replace('#/onAdvancedSearch');
					 location.path('/onAdvancedSearch');
				 } 
			 }
		  	}
		  };

}]);