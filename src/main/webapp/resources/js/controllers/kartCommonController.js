var controllers =  angular.module('kartCommonsModule');

/*controllers.controller('KartAppDataCtrl', function(){
	
});*/
controllers.controller('BaseFormCtrl', ['$scope','$rootScope', '$http', function ($scope,$rootScope,$http) {
    $scope.vm = {
        submitted: false,
        errorMessages: []
    };
}]);
controllers.controller('KartAppDataCtrl', ['$scope','$localStorage', 'filterFilter', 'KartAppDataService', function ObjectArrayCtrl($scope, $localStorage, filterFilter, KartAppDataService) {
	  // Categories
	$scope.categories = [];
	 // selected categories
	$scope.vm.selectionCat = [];
	//Metal Types
	 $scope.metalTypes = [];
	 // selected MetalTypes
	$scope.vm.selectionMet = [];
	
	 $scope.countries = KartAppDataService.getCountry();

     $scope.getCountryStates = function(){
    	 $scope.vm.shopState = null;
         $scope.states = KartAppDataService.getCountryState($scope.vm.shopCountry);
         
 
     };
	//Cload Category checkbox list
	$scope.loadCategories = function(){
	  $scope.categories = [
	                    { name: 'HAIR',  id:101,  selected: false },
		           	    { name: 'HEAD', id:102,  selected: false },
		           	    { name: 'NOSE', id:103,    selected: false },
		           	    { name: 'EAR', id:104, selected: false },
		           	    { name: 'NECK', id:105, selected: false },
		           	    { name: 'UPPER ARM', id:106, selected: false },
		           	    { name: 'ARM', id:107,  selected: false },
		           	    { name: 'FINGER', id:108, selected: false },
		           	    { name: 'WAIST', id:109, selected: false },
		           	    { name: 'LEG', id:110,  selected: false },
		           	    { name: 'DECORATION', id:111, selected: false },
		           	    { name: 'RELIGIOUS', id:112, selected: false },
		           	    { name: 'BARS', id:113, selected: false },
		           	    { name: 'COINS', id:114, selected: false },
		           	    { name: 'OTHER', id:115, selected: false }
	  ];
	};
	 

	  // helper method to get selected categories
	  $scope.selectedcategories = function selectedcategories() {
	    return filterFilter($scope.categories, { selected: true });
	  };

	  // watch categories for changes
	  $scope.$watch('categories|filter:{selected:true}', function (nv) {
	    $scope.vm.selectionCat = nv.map(function (category) {
	      return category.id;
	    });
	  }, true);
	  
	//Load Category checkbox list
	  $scope.loadMetalTypes = function(){
		  $scope.metalTypes = [
		           	    { name: 'GOLD', id:10,   selected: false },
		           	    { name: 'SILVER', id:20,   selected: false },
		           	    { name: 'ALLOY',  id:30,   selected: false },
		           	    { name: 'BRASS', id:40, selected: false },
		           	    { name: 'PALLADIUM', id:50, selected: false }
		           	  ];
	  };
	  
	  // helper method to get selected MetalTypes
	  $scope.selectedMetalTypes = function selectedMetalTypes() {
	    return filterFilter($scope.metalTypes, { selected: true });
	  };

	  // watch MetalTypes for changes
	  $scope.$watch('metalTypes|filter:{selected:true}', function (nv) {
	    $scope.vm.selectionMet = nv.map(function (metalType) {
	      return metalType.id;
	    });
	  }, true);
	  
	//Get Data from Localstorage
	  $scope.getRecentItems = function(){
		 $scope.recentItems = $localStorage.savedItems; 
		 //$scope.cachedSlides = $localStorage.savedItems; 
	  };
	  
	 //Used to populate carousel	  
	  $scope.$watch('recentItems', function(values) {

	    var i, a = [], b;
	    if($scope.recentItems != null && $scope.recentItems.length > 0){
		    for (i = 0; i < $scope.recentItems.length; i += 3) {
		      b = { item1: $scope.recentItems[i] };
	
		      if ($scope.recentItems[i + 1]) {
		        b.item2 = $scope.recentItems[i + 1];
		      }
		      if ($scope.recentItems[i + 2]) {
			        b.item3 = $scope.recentItems[i + 2];
			      }
		      if ($scope.recentItems[i + 3]) {
			        b.item4 = $scope.recentItems[i + 3];
			      }
		       
	
		      a.push(b);
		    }
	  }
	    $scope.cachedItemSlides = a;

	  }, true);
	  
	}]);
controllers.controller('ScrollController', ['$scope', '$location', '$anchorScroll',
    function ($scope, $location, $anchorScroll) {
        $scope.gotoTop = function() {
            // set the location.hash to the id of
            // the element you wish to scroll to.
            $location.hash('topnavbar');

            // call $anchorScroll()
            $anchorScroll();
        };
}]);