var controllers =  angular.module('kartItemDetailsModule');

/*controllers.controller('KartItemDetailsCtrl', function(){
	
});*/
controllers.controller('KartItemCtrl',['ItemService','MerchantInfoService','ItemDataService','$modal','$rootScope', '$scope', '$localStorage', '$routeParams', '$route','$location',function(ItemService, MerchantInfoService,ItemDataService, $modal, $rootScope, $scope, $localStorage, $routeParams, $route,$location){
	var businessName = $routeParams.businessName;
	
	$scope.findItemsByBusinessName = function(){
		ItemService.findItemsByBusinessName(businessName,function(response){
            if (response.success) {
            	$scope.merchantItems = response.data;
            	MerchantInfoService.updateInfo(response.data, businessName);
            	$rootScope.merchantShopName = businessName;
            	
            	$scope.logoImage = MerchantInfoService.merchant.logoImage;
           	  	$scope.businessName = MerchantInfoService.merchant.businessName;
        	   	$scope.userId = MerchantInfoService.merchant.userId;
        	   	$scope.logoImage = MerchantInfoService.merchant.merchantLogo;
        	   	$scope.businessName = MerchantInfoService.merchant.businessName;
        	   	$scope.marketingName = MerchantInfoService.merchant.marketingName;
        	   	$scope.marketingEmail = MerchantInfoService.merchant.marketingEmail;
        	   	$scope.marketingPhone1 = MerchantInfoService.merchant.marketingPhone1;
        	   	$scope.marketingPhone2 = MerchantInfoService.merchant.marketingPhone2;
			 } else {
				 MerchantInfoService.updateInfo(null, null);
			    $scope.vm.errorMessages = [];
			    $scope.vm.errorMessages.push({description: response.message});
			    $scope.test=response.message;
			    $rootScope.merchantShopName = businessName;
			 }
		}); 
	};
	
	$scope.getItemdetails = function(_itemSummary,businessName){
		
		ItemService.getItemDetailsBysourceId(_itemSummary.itemId,function(response){
            if (response.success) {
            	
            	ItemDataService.update(response.data,businessName);
            	if($localStorage.savedItems == null){
            		$localStorage.savedItems = [];
            		$localStorage.savedItems.push(_itemSummary);
            	}else {
            		var matches=false;
            		 // Make sure user has not already added this item
            	    angular.forEach($localStorage.savedItems, function(item) {
            	        if (_itemSummary.itemId === item.itemId) {
            	            matches = true;
            	        }
            	    });
            	    if(matches === false){
            	    	$localStorage.savedItems.push(_itemSummary);
            	    }
            	}
            	
            	//window.location.replace('#/itemDetails');
            	$location.path("/itemDetails");
			 } else {
			    $scope.vm.errorMessages = [];
			    $scope.vm.errorMessages.push({description: response.message});
			    ItemDataService.update(null,null);
			 }
		});
		
	};
	$scope.getItemDetails = function(){
		$scope.kartItemDetailsDto = ItemDataService.kartItemDetailsDto;
	};
}]);
