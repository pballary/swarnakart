var controllers =  angular.module('kartMerchantModule');

controllers.controller('KartMerchantCtrl', ['$scope','$rootScope','$window','$filter','KartMerchantService','KartAppDataService','MerchantInfoService', function($scope, $rootScope, $window, $filter, KartMerchantService,KartAppDataService,MerchantInfoService){
	$scope.getRandomMerchants= function(){
		KartMerchantService.getRandomMerchants(function (response) {
			if (response.success) {
				$scope.merchantList = response.data;
			   
			 } else {
			    $scope.vm.errorMessages = [];
			     $scope.vm.errorMessages.push({description: response.message});
			     $scope.test=response.message;
			 }
		});
	};
	 $scope.showUserModal = function(idx){
		    var merchant = $scope.merchantList[idx];
		    $scope.currUser = merchant;
		    $scope.currUser.title= "Click to view all items from "+merchant.businessName;
		    var countries = KartAppDataService.getCountry();
		    var selectedCounty = null;
		    var selectedState = null;
	        if(merchant.country != null){
	        	selectedCounty = $filter('filter')(countries, {code:merchant.country})[0];
	        	if(selectedCounty != null){
	        		states = KartAppDataService.getCountryState(selectedCounty.code);
	        		selectedState = $filter('filter')(states, {code:merchant.state})[0];
	        		$scope.currUser.country = selectedCounty.name;
	        		$scope.currUser.state = selectedState.name;
	        	}
	        	
	        	
	        }
	        
		    $('#myModalLabel').text(merchant.businessName);
		    $('#myModal').modal('show');
	 };
	 
	 $scope.getMerchantLogo = function(){
			$scope.vm.logoImage = MerchantInfoService.merchant.logoImage;
	   	  	$scope.vm.businessName = MerchantInfoService.merchant.businessName;
		   	$scope.vm.userId = MerchantInfoService.merchant.userId;
		   	$scope.vm.logoImage = MerchantInfoService.merchant.merchantLogo;
		   	$scope.vm.businessName = MerchantInfoService.merchant.businessName;
		   	$scope.vm.marketingName = MerchantInfoService.merchant.marketingName;
		   	$scope.vm.marketingEmail = MerchantInfoService.merchant.marketingEmail;
		   	$scope.vm.marketingPhone1 = MerchantInfoService.merchant.marketingPhone1;
		   	$scope.vm.marketingPhone2 = MerchantInfoService.merchant.marketingPhone12;
	     };
		  
	     $scope.openMerchantHomePage = function(){
	    	 var bussName = $scope.kartItemDetailsDto.businessName;
			 $window.open('https://www.swarnakart.com/'+bussName, '_blank');
		};  
		 $scope.openMerchantHomePageFromPopup = function(){
	    	 var bussName =  $scope.currUser.businessName;
			 $window.open('https://www.swarnakart.com/'+bussName, '_blank');
		};  
}]);