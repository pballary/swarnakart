var kartServices =  angular.module('kartItemDetailsServiceModule');

/*kartServices.service('KartItemDetailsService', function(){
	
});*/
kartServices.service('ItemService', ['$http', 'KartAppDataService', function($http, KartAppDataService) {

	
	this.findItemsByBusinessName = function(businessName,callback) {
 		$http({
            method: 'GET',
            url: KartAppDataService.getBasePath() + "api/items/itemsByBusinessName",
            params : {
            	businessName : businessName
 			},
             headers: {
                 "Content-Type": "application/json"
             }
        }).then(function (response) {
            if (response.status == 200) {
           	 response = { success: true, data:response.data };
            }
            else {
           	 response = { success: false, message: 'Unable to get Items. Please try after some time.' };
            }
            callback(response);
            
        }, function errorCallback(response) {
            // called asynchronously if an error occurs
            // or server returns response with an error status.
        	 response = { success: false, message: response.data.errorMsg };
        	 callback(response);
        });
	};
	
    this.getItemDetailsBysourceId = function(sourceId, callback) {
 		   
 		  $http({
 	            method: 'GET',
 	            url: KartAppDataService.getBasePath() + "api/items/getItemDetailById",
 	            params : {
 	            	sourceId : sourceId
 	 			},
 	             headers: {
 	                 "Content-Type": "application/json"
 	             }
 	        }) 
 		   .then(function (response) {
	          if (response.status == 200) {
	         	 response = { success: true, data:response.data };
	          }
	          else {
	         	 response = { success: false, message: 'Unable to get details. Please try after some time.' };
	          }
	          callback(response);
	          
	      });
	    };
	
}]);


kartServices.service('ItemDataService', function () {
    return {
    	kartItemDetailsDto: null,
        update: function(kartItemDetailsDto) {
          this.kartItemDetailsDto = kartItemDetailsDto;
          
        }
      };
});