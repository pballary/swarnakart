var kartServices =  angular.module('kartSearchServiceModule');

kartServices.service('KartSearchService', ['$http','KartAppDataService', function($http,KartAppDataService){
	 this.findTopTrendingItems = function(callback) {
		 $http({
	            method: 'GET',
	            url:KartAppDataService.getBasePath() + "api/items/findAllItems",
                headers: {
	                 "Content-Type": "application/json"
	             }
	        }).then(function (response) {
             if (response.status == 200) {
            	 response = { success: true, data:response.data };
             }
             else {
            	 response = { success: false, message: 'Unable to get Items. Please try after some time.' };
             }
             callback(response);
             
         });
	};
	
	this.searchByCriteria = function(searchCriteria, callback) {
		
		$http({
            method: 'GET',
            url: KartAppDataService.getBasePath() + "api/items/getItemsByCriteria",
            params : {
            	itemSearch : searchCriteria
 			},
             headers: {
                 "Content-Type": "application/json"
             }
        }) .then(function (response) {
            if (response.status == 200) {
           	 response = { success: true, data:response.data };
            }
            else {
           	 response = { success: false, message: 'Unable to get Items. Please try after some time.' };
            }
            callback(response);
            
        });
	};
}]);