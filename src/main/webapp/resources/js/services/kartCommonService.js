var kartServices =  angular.module('kartCommonServiceModule');

kartServices.service('KartAppDataService', function(){
	var countrylist = [{ "name": "United States", "code": "US"},{ "name": "India", "code": "IN" }];
	var usStates = [{"name":"Alabama","code":"AL"},{"name":"Alaska","code":"AK"},{"name":"Arizona","code":"AZ"},{"name":"Arkansas","code":"AR"},{"name":"California","code":"CA"},{"name":"Colorado","code":"CO"},{"name":"Connecticut","code":"CT"},{"name":"Delaware","code":"DE"},{"name":"District of Columbia","code":"DC"},{"name":"Florida","code":"FL"},{"name":"Georgia","code":"GA"},{"name":"Hawaii","code":"HI"},{"name":"Idaho","code":"ID"},{"name":"Illinois","code":"IL"},{"name":"Indiana","code":"IN"},{"name":"Iowa","code":"IA"},{"name":"Kansas","code":"KS"},{"name":"Kentucky","code":"KY"},{"name":"Louisiana","code":"LA"},{"name":"Maine","code":"ME"},{"name":"Maryland","code":"MD"},{"name":"Massachusetts","code":"MA"},{"name":"Michigan","code":"MI"},{"name":"Minnesota","code":"MN"},{"name":"Mississippi","code":"MS"},{"name":"Missouri","code":"MO"},{"name":"Montana","code":"MT"},{"name":"Nebraska","code":"NE"},{"name":"Nevada","code":"NV"},{"name":"New Hampshire","code":"NH"},{"name":"New Jersey","code":"NJ"},{"name":"New Mexico","code":"NM"},{"name":"New York","code":"NY"},{"name":"North Carolina","code":"NC"},{"name":"North Dakota","code":"ND"},{"name":"Ohio","code":"OH"},{"name":"Oklahoma","code":"OK"},{"name":"Oregon","code":"OR"},{"name":"Pennsylvania","code":"PA"},{"name":"Rhode Island","code":"RI"},{"name":"South Carolina","code":"SC"},{"name":"South Dakota","code":"SD"},{"name":"Tennessee","code":"TN"},{"name":"Texas","code":"TX"},{"name":"Utah","code":"UT"},{"name":"Vermont","code":"VT"},{"name":"Virginia","code":"VA"},{"name":"Washington","code":"WA"},{"name":"West Virginia","code":"WV"},{"name":"Wisconsin","code":"WI"},{"name":"Wyoming","code":"WY"}];
	var indianStates =[{"code":"AP","name":"Andhra Pradesh"},{"code":"AR","name":"Arunachal Pradesh"},{"code":"AS","name":"Assam"},{"code":"BR","name":"Bihar"},{"code":"CG","name":"Chhattisgarh"},{"code":"Chandigarh","name":"Chandigarh"},{"code":"DN","name":"Dadra and Nagar Haveli"},{"code":"DD","name":"Daman and Diu"},{"code":"DL","name":"Delhi"},{"code":"GA","name":"Goa"},{"code":"GJ","name":"Gujarat"},{"code":"HR","name":"Haryana"},{"code":"HP","name":"Himachal Pradesh"},{"code":"JK","name":"Jammu and Kashmir"},{"code":"JH","name":"Jharkhand"},{"code":"KA","name":"Karnataka"},{"code":"KL","name":"Kerala"},{"code":"MP","name":"Madhya Pradesh"},{"code":"MH","name":"Maharashtra"},{"code":"MN","name":"Manipur"},{"code":"ML","name":"Meghalaya"},{"code":"MZ","name":"Mizoram"},{"code":"NL","name":"Nagaland"},{"code":"OR","name":"Orissa"},{"code":"PB","name":"Punjab"},{"code":"PY","name":"Pondicherry"},{"code":"RJ","name":"Rajasthan"},{"code":"SK","name":"Sikkim"},{"code":"TN","name":"Tamil Nadu"},{"code":"TL","name":"Telangana"},{"code":"TR","name":"Tripura"},{"code":"UP","name":"Uttar Pradesh"},{"code":"UK","name":"Uttarakhand"},{"code":"WB","name":"West Bengal"}];
	var basePath="";
	
	// Get Country list
	this.getCountry = function(){    
	    return countrylist;
	};

	// Get State list for country
	this.getCountryState = function(countryCode){
		var states = [];
		if(countryCode === 'US'){
			states = usStates;
		}else if(countryCode === 'IN'){
			states = indianStates;
		}
	    return states;
	};
	this.getBasePath = function(){
		return basePath;
	};
});