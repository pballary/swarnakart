var kartServices =  angular.module('kartMerchantServiceModule');

kartServices.service('KartMerchantService', ['$http','KartAppDataService', function($http,KartAppDataService){
	
	 this.getRandomMerchants = function(callback) {
		 $http({
	            method: 'GET',
	            url:KartAppDataService.getBasePath() + "api/merchants/getRandomMerchants",
             headers: {
	                 "Content-Type": "application/json"
	             }
	        }).then(function (response) {
             if (response.status == 200) {
            	 response = { success: true, data:response.data };
             }
             else {
            	 response = { success: false, message: 'Unable to get Merchants. Please try after some time.' };
             }
             callback(response);
             
         });
	};
	
	
}]);
kartServices.service('MerchantInfoService', function () {
	return {
		    merchant: {
		      userId: '',
		      logoImage: null,
		      businessName:'',
		      marketingName:'',
		      marketingEmail:'',
		      marketingPhone1:'',
		      marketingPhone2:''
		    },
		    updateId: function(id) {
		      this.merchant.userId = id;
		      
		    },
		    updateLogo: function(logo) {
			      this.merchant.logoImage = logo;
			      
			},
		   /* updateInfo: function(id,logo,businessName) {
			      this.merchant.userId = id;
			      this.merchant.logoImage = logo;
			      this.merchant.businessName = businessName;
		    }*/
			updateInfo: function(data,businessName) {
				if(data != null){
			      this.merchant.userId = data.userId;
			      this.merchant.logoImage = data.merchantLogo;
			      this.merchant.businessName = data.businessName;
			      this.merchant.marketingName = data.marketingName;
			      this.merchant.marketingEmail = data.marketingEmail;
			      this.merchant.marketingPhone1 = data.marketingPhone1;
			      this.merchant.marketingPhone2 = data.marketingPhone2;
				}else{
					this.merchant = null;
				}
		    }
	  };
});