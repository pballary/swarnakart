var swarnaKartApp = angular.module('swarnakartApp',['kartCommonsModule','kartSearchModule','kartSearchModule','kartUserModule','kartMerchantModule','kartItemDetailsModule', 'kartItemDetailsServiceModule',
                                                    'kartMerchantServiceModule','kartSearchServiceModule','kartUserServiceModule','kartCommonServiceModule','ngMessages','ngStorage','ngRoute','ui.bootstrap','chieffancypants.loadingBar']);
angular.module('kartCommonsModule',[]);
angular.module('kartSearchModule',[]);
angular.module('kartUserModule',[]);
angular.module('kartMerchantModule',[]);
angular.module('kartItemDetailsModule',[]);
angular.module('kartCommonServiceModule',[]);
angular.module('kartItemDetailsServiceModule',[]);
angular.module('kartMerchantServiceModule',[]);
angular.module('kartSearchServiceModule',[]);
angular.module('kartUserServiceModule',[]);

swarnaKartApp.directive('toggle', function(){
	  return {
	    restrict: 'A',
	    link: function(scope, element, attrs){
	      if (attrs.toggle=="tooltip"){
	        $(element).tooltip();
	      }
	      if (attrs.toggle=="popover"){
	        $(element).popover();
	      }
	    }
	  };
	});

swarnaKartApp.directive('backButton', function(){
    return {
        restrict: 'A',
        link: function(scope, element, attrs) {
            element.bind('click', function () {
                history.back();
                scope.$apply();
            });
        }
    };
});
swarnaKartApp.config(['cfpLoadingBarProvider', function(cfpLoadingBarProvider) {
    //cfpLoadingBarProvider.latencyThreshold = 500;
	//cfpLoadingBarProvider.includeBar = false;
	cfpLoadingBarProvider.includeSpinner = false;
  }]);
swarnaKartApp.config(['$routeProvider', '$httpProvider','$locationProvider',
                  function ($routeProvider, $httpProvider,$locationProvider) {
                      $routeProvider
	                      .when('/itemDetails', {
	                          templateUrl: 'views/items/kartItemDetails.html',
	                          //controller: 'ItemController'
	                      }).when('/all', {
                              templateUrl: 'views/items/home.html'
                          })
                          /* .when('/', {
                              templateUrl: 'views/items/kartAllItems.html',
                              controller: 'AllItemController'
                          })*/
                           .when('/onAdvancedSearch', {
                        	   templateUrl: 'views/home.html',
                        	   controller: 'KartSearchCtrl'
                          })
                          .when('/onSimpleSearch', {
                        	   templateUrl: 'views/home.html',
                        	   controller: 'KartSearchCtrl'
                          }).when('/terms', {
                              title: 'Terms and Conditions',
                              templateUrl: 'views/termsandconditions.html'
                          })
                          .when('/:businessName', {
                              templateUrl: 'views/items/kartItemList.html',
                              controller: 'KartItemCtrl'
                          })                         
                          /* .when('/', {
                        	   templateUrl: 'views/home.html'
                          })*/
                          .otherwise({
                        	  templateUrl: 'views/home.html'
                          });
                      // use the HTML5 History API
                      $locationProvider.html5Mode(true);
                      
                  }])
                  .run(['$rootScope', '$location', '$http',
                      function($rootScope, $location, $http) {
                          $rootScope.$on('$routeChangeStart', function (event, next) {
                          	if(!next.$$route){
                          		$rootScope.currentPage = '/all';
                          	} else {
                          		$rootScope.currentPage = next.$$route.originalPath;
                          	}
                          });

                  }]);

