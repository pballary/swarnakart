package com.sm.swarnakart.exception;


public class ApiException extends RuntimeException{
    /**
	 * 
	 */
	private static final long serialVersionUID = 4300948954313910259L;
	private int errorCode;
    private String message;


    public ApiException() {
        super();
    }

    /*public ApiException(int errorCode) {
        super(ErrorCodes.getMessage(errorCode));
        this.errorCode = errorCode;
    }*/

    public ApiException(int errorCode, String message) {
        super(message);
        this.errorCode = errorCode;
    }
    public ApiException(String message) {
        super(message);
    }

    public ApiException(String message, Throwable cause) {
        super(message, cause);
    }

    public ApiException(Throwable cause) {
        super(cause);
    }

    public ApiException(int errorCode, String message, Throwable cause) {
        super(message, cause);
        this.errorCode = errorCode;
    }
    
    public int getErrorCode() {
        return errorCode;
    }
}
