package com.sm.swarnakart.exception;

public class ValidationException extends ApiException {

   /* public ValidationException(int errorCode) {
        super(errorCode);
    }*/

    /**
	 * 
	 */
	private static final long serialVersionUID = 3494526129849002206L;

	public ValidationException(int errorCode, String message) {
        super(errorCode, message);
    }

    public ValidationException(String message) {
        super(message);
    }

    public ValidationException() {
    }

    public ValidationException(String message, Throwable cause) {
        super(message, cause);
    }

    public ValidationException(Throwable cause) {
        super(cause);
    }
}
