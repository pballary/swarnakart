package com.sm.swarnakart.services;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.persistence.NoResultException;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.sm.swarnakart.dto.ItemSummaryDto;
import com.sm.swarnakart.dto.KartItemSearchDto;
import com.sm.swarnakart.dto.MerchantItemDto;
import com.sm.swarnakart.dto.kartItemDetailsDto;
import com.sm.swarnakart.entities.Document;
import com.sm.swarnakart.entities.ItemDetail;
import com.sm.swarnakart.entities.ItemSource;
import com.sm.swarnakart.entities.LuDeliveryOptions;
import com.sm.swarnakart.entities.Merchant;
import com.sm.swarnakart.entities.MerchantLogin;
import com.sm.swarnakart.exception.ValidationException;
import com.sm.swarnakart.repositories.KartFileSystemDocumentRepository;
import com.sm.swarnakart.repositories.KartItemRepository;

@Service
public class KartItemService {
	
	@Autowired
	private KartItemRepository itemRepository;
	@Autowired
	private KartFileSystemDocumentRepository documentRepository;
	
	private static Logger logger = LoggerFactory.getLogger(KartItemService.class);
	
    @Value("${cloudinary.baseURL}")
    private String cloudinaryBaseUrl;
	
	public List<ItemSummaryDto> findAllItems(){
		logger.info("Entering findAllItems");
		ItemSummaryDto itemSummaryDto = null;
		List<ItemSummaryDto> itemList = new ArrayList<ItemSummaryDto>();
		List<ItemSource> itemSourceList = itemRepository.findAllItems();
		for (ItemSource itemSource : itemSourceList) {
			itemSummaryDto = new ItemSummaryDto();
			itemSummaryDto.setItemDetail(itemSource.getItemDetail());
			populateItemDetails(itemSummaryDto, itemSource);
			itemList.add(itemSummaryDto);
		}
		logger.info("Total items found:"+itemList.size());
		logger.info("Exiting findAllItems");
		return itemList;
	}

	public MerchantItemDto findItemsbyBusinessName(String businessName){
		logger.info("Entering findItemsbyBusinessName");
		MerchantItemDto merchantItemDto = new MerchantItemDto();
		ItemSummaryDto itemSummaryDto = null;
		MerchantLogin merchantLogin = null;
		Document document = null;
		try{
		merchantLogin = itemRepository.findByShopName(businessName);
		}catch(NoResultException ne){
			throw new ValidationException("Invalid Business name.");
		}
		if(merchantLogin == null){
			logger.info("Invalid Business name");
			throw new ValidationException("Invalid Business name.");
		}
		merchantItemDto.setMerchantLoginId(merchantLogin.getId());
		merchantItemDto.setBusinessName(merchantLogin.getShopName());
		merchantItemDto.setUserId(merchantLogin.getUserdId());
		merchantItemDto.setMarketingEmail(merchantLogin.getMerchant().getMarketingEmail());
		merchantItemDto.setMarketingName(merchantLogin.getMerchant().getMarketingPersonName());
		merchantItemDto.setMarketingPhone1(merchantLogin.getMerchant().getMarketingPersonPhoneM1());
		merchantItemDto.setMarketingPhone2(merchantLogin.getMerchant().getMarketingPersonPhoneM2());
		
		if(merchantLogin.getMerchant() == null){
			logger.info("Merchant Profile is not completed");
			throw new ValidationException("Merchant Profile is not completed.");
		}
		if(merchantLogin.getMerchant().getItemSources().isEmpty()){
			logger.info("No items found for "+ merchantLogin.getShopName());
			throw new ValidationException("No items found for "+ merchantLogin.getShopName());
		}
		if(!StringUtils.isBlank(merchantLogin.getMerchant().getShopLogoFileName())){
			merchantItemDto.setLogoURI(cloudinaryBaseUrl+merchantLogin.getMerchant().getShopLogoFileName());
		}
		/*document = documentRepository.load(merchantLogin.getMerchant().getShopLogoFileName());
		
		if(document != null && document.getFileData()!= null && document.getFileData().length>0){
			merchantItemDto.setMerchantLogo(document.getFileData());
		}*/
		if(merchantLogin.getMerchant().getItemSources() != null && !merchantLogin.getMerchant().getItemSources().isEmpty()){
			for (ItemSource itemSource : merchantLogin.getMerchant().getItemSources()) {
				ItemDetail itemDetail = itemSource.getItemDetail();
				itemSummaryDto = new ItemSummaryDto();
				itemSummaryDto.setItemDetail(itemDetail);
				populateItemDetails(itemSummaryDto,itemSource);
				merchantItemDto.getItemSummaryDtos().add(itemSummaryDto);
			}
		}
		logger.info("Exiting findItemsbyBusinessName");
		return merchantItemDto;
	}
	
	public List<ItemSummaryDto> findItemsByCriteria(KartItemSearchDto itemSearchDto){
		logger.info("Entering findAllItems");
		ItemSummaryDto itemSummaryDto = null;
		List<ItemSummaryDto> itemList = new ArrayList<ItemSummaryDto>();
		List<ItemSource> itemSourceList = itemRepository.findItemsByCriteria(itemSearchDto);
		for (ItemSource itemSource : itemSourceList) {
			itemSummaryDto = new ItemSummaryDto();
			itemSummaryDto.setItemDetail(itemSource.getItemDetail());
			populateItemDetails(itemSummaryDto, itemSource);
			itemList.add(itemSummaryDto);
		}
		logger.info("Total items found:"+itemList.size());
		logger.info("Exiting findAllItems");
		return itemList;
	}
	public kartItemDetailsDto getItemDetailsById(int itemSourceId){
		logger.info("Entering getItemDetailsById");
		ItemSource itemSource = null;
		byte[] itemImage = null;
		byte[] logoImage = null;
		kartItemDetailsDto kartItemDetailsDto = new kartItemDetailsDto();
		itemSource = itemRepository.findByItemId(itemSourceId);
		Merchant merchant = itemSource.getMerchant();
		if(itemSource != null && itemSource.getItemDetail() != null && merchant != null ){
			kartItemDetailsDto.setItemDetail(itemSource.getItemDetail());
			kartItemDetailsDto.setBusinessName(merchant.getMerchantLogin().getShopName());
			kartItemDetailsDto.setShopFullName(merchant.getShopAddressLn1());
			kartItemDetailsDto.setMarketingEmail(merchant.getMarketingEmail());
			kartItemDetailsDto.setMarketingName(merchant.getMarketingPersonName());
			kartItemDetailsDto.setMarketingPhone1(merchant.getMarketingPersonPhoneM1());
			kartItemDetailsDto.setMarketingPhone2(merchant.getMarketingPersonPhoneM2());
			logoImage = getDocumentFile(merchant.getShopLogoFileName());
			kartItemDetailsDto.setMerchantLogo(logoImage);
			//itemImage = getDocumentFile(itemSource.getItemDetail().getPhoto2());
			//kartItemDetailsDto.setItemImage(itemImage);
			 Set<LuDeliveryOptions> deliveryOptions = itemSource.getItemDetail().getDeliveryOffered();
	        Set<String> offeredDeliveryOptions = new HashSet<>();
	        for (LuDeliveryOptions deliveryOption : deliveryOptions) {
	            offeredDeliveryOptions.add(deliveryOption.getDeliveryValue());
	        }
	        kartItemDetailsDto.setDeliveryOffered(offeredDeliveryOptions);
			kartItemDetailsDto.setImageUUID(cloudinaryBaseUrl+itemSource.getItemDetail().getPhoto2());
		}
		
		logger.info("Exiting getItemDetailsById");
		return kartItemDetailsDto;
		
		
	}
	private void populateItemDetails(ItemSummaryDto itemSummaryDto,	ItemSource itemSource) {
		logger.info("Entering populateItemDetails");
		byte[] image = null;
		itemSummaryDto.setSrcId(itemSource.getItemId());
		itemSummaryDto.setItemId(itemSource.getItemDetail().getItemId());
		itemSummaryDto.setArticleType(itemSource.getItemDetail().getArticleOtherTypeDesc() != null?itemSource.getItemDetail().getArticleOtherTypeDesc():itemSource.getItemDetail().getLuArticlesOffered().getArticlesOffered());
		itemSummaryDto.setMetalType(itemSource.getItemDetail().getLuMetalsOffered() == null?"":itemSource.getItemDetail().getLuMetalsOffered().getMetalsOffered());
		itemSummaryDto.setTotalPrice(itemSource.getItemDetail().getTotalPrice());
		itemSummaryDto.setCurrency((itemSource.getItemDetail().getCurrency().equalsIgnoreCase("USD")?"$":"Rs."));
		itemSummaryDto.setUpdateDate(itemSource.getUpdateDate());
		itemSummaryDto.setPhoto1UUID(cloudinaryBaseUrl+itemSource.getItemDetail().getPhoto1());
		itemSummaryDto.setPhoto2UUID(cloudinaryBaseUrl+itemSource.getItemDetail().getPhoto2());
		/*image = getDocumentFile(itemSource.getItemDetail().getPhoto1());
		if(image != null && image.length>0){
			itemSummaryDto.setPhoto1(image);
		}*/
		logger.info("Exiting populateItemDetails");
	}

	public byte[] getDocumentFile(String id) {
		logger.info("Entering getDocumentFile");
        Document document = documentRepository.load(id);
        if(document!=null) {
        	logger.info("Exiting getDocumentFile: Found an image");
            return document.getFileData();
        } else {
        	logger.info("Exiting getDocumentFile: Image not found");
            return null;
        }
    }
}
