package com.sm.swarnakart.services;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.sm.swarnakart.dto.MerchantItemDto;
import com.sm.swarnakart.entities.Document;
import com.sm.swarnakart.entities.Merchant;
import com.sm.swarnakart.repositories.KartFileSystemDocumentRepository;
import com.sm.swarnakart.repositories.KartMerchantRepository;

@Service
public class KartMerchantService {
	
	@Autowired
	private KartMerchantRepository merchantRepository;
	@Autowired
	private KartFileSystemDocumentRepository documentRepository;
	
	@Value("${cloudinary.baseURL}")
	private String cloudinaryBaseUrl;
	
	private static Logger logger = LoggerFactory.getLogger(KartMerchantService.class);
	
	/**
	 * This method returns random list of merchants for specified number of records.
	 * @param maxRecordCount
	 * @return
	 */
	public List<MerchantItemDto> findRandomMerchants(int maxRecordCount){
		logger.info("Entering findRandomMerchants()");
		List<MerchantItemDto> merchantItemDtos = new ArrayList<MerchantItemDto>();
		List<Merchant> merchants = merchantRepository.getRandomMerchantList(maxRecordCount);
		for (Merchant merchant : merchants) {
			buildMerchantDetails(merchantItemDtos, merchant);
		}
		logger.info("Exiting findRandomMerchants()");
		return merchantItemDtos;
	}


	private void buildMerchantDetails(List<MerchantItemDto> merchantItemDtos, Merchant merchant) {
		MerchantItemDto merchantItemDto;
		merchantItemDto = new MerchantItemDto();
		merchantItemDto.setMerchantId(merchant.getId());
		merchantItemDto.setBusinessName(merchant.getMerchantLogin().getShopName());
		merchantItemDto.setMarketingEmail(merchant.getMarketingEmail());
		merchantItemDto.setMarketingName(merchant.getMarketingPersonName());
		merchantItemDto.setMarketingPhone1(merchant.getMarketingPersonPhoneM1());
		merchantItemDto.setMarketingPhone2(merchant.getMarketingPersonPhoneM2());
		merchantItemDto.setAddressLine1(merchant.getShopAddressLn1());
		merchantItemDto.setAddressLine2(merchant.getShopAddressLn2());
		merchantItemDto.setCountry(merchant.getShopCountry());
		merchantItemDto.setState(merchant.getShopState());
		merchantItemDto.setCity(merchant.getShopCity());
		
		if(!StringUtils.isBlank(merchant.getShopLogoFileName())){
			//merchantItemDto.setMerchantLogo(getDocumentFile(merchant.getShopLogoFileName()));
			merchantItemDto.setLogoURI(cloudinaryBaseUrl+merchant.getShopLogoFileName());
		}else{
			merchantItemDto.setLogoURI("/resources/img/Swarna-Black-Logo.png");
		}
		merchantItemDtos.add(merchantItemDto);
	}
	
	
	/**
	 * This method fetches based on Image UUID
	 * @param id
	 * @return
	 */
	public byte[] getDocumentFile(String id) {
		logger.info("Entering getDocumentFile");
        Document document = documentRepository.load(id);
        if(document!=null) {
        	logger.info("Exiting getDocumentFile: Found an image");
            return document.getFileData();
        } else {
        	logger.info("Exiting getDocumentFile: Image not found");
            return null;
        }
    }
}
