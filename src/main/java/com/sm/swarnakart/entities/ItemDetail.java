package com.sm.swarnakart.entities;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.MapsId;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import org.codehaus.jackson.annotate.JsonBackReference;


/**
 * The persistent class for the item_details database table.
 * 
 */
@Entity
@Table(name="item_details")
@NamedQueries({
	@NamedQuery(name="ItemDetail.findAll", query="SELECT i FROM ItemDetail i"),
	@NamedQuery(name="ItemDetail.findBySourceId", query="SELECT i FROM ItemDetail i where itemSrcId =:itemSrcId")
})

public class ItemDetail implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	//@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name="item_id")
	private int itemId;

	/*@Column(name="article_type")
	private String articleType;*/

	private String availability;

	@Column(name="AVAILABLE_SIZES")
	private int availableSizes;

	private String brand;

	private int customizable;

    @ManyToMany
    @JoinTable(name = "item_details_delivery_options", joinColumns = { @JoinColumn(name = "item_details_item_id") }, inverseJoinColumns = { @JoinColumn(name = "lu_delivery_options_ID") })
	private Set<LuDeliveryOptions> deliveryOffered;

	private String description;

	private String design;

	@Column(name="DESIGN_3")
	private String design3;

	private String dimension;

	@Column(name="DIMOND_STONE_CARATS")
	private BigDecimal dimondStoneCarats;

	@Column(name="DIMOND_STONE_CERTIFICATION")
	private String dimondStoneCertification;

	@Column(name="DIMOND_STONE_CLARITY")
	private String dimondStoneClarity;

	@Column(name="DIMOND_STONE_COLOUR")
	private String dimondStoneColour;

	@Column(name="DIMOND_STONE_PIECES")
	private int dimondStonePieces;

	@Column(name="FACEBOOK_LIKES")
	private int facebookLikes;

	@Column(name="FACEBOOK_SHARE_LINK")
	private String facebookShareLink;

	/*private BigDecimal fee;*/

	private String gender;

	private BigDecimal height;

	private BigDecimal length;

	/*@Column(name="LU_ARTICLES_TYPE")
	private int luArticlesType;*/

	@Column(name="lu_item_type")
	private String luItemType;
	/*
	@Column(name="LU_METALS_TYPE")
	private int luMetalsType;*/

	@Column(name="LU_METALS_TYPE_COLOUR")
	private String luMetalsTypeColour;

	private String LU_METALS_TYPE_GROSS_Weight;

	@Column(name="LU_METALS_TYPE_NET_CERTIFICATION")
	private String luMetalsTypeNetCertification;

	private BigDecimal LU_METALS_TYPE_NET_Weight;

	@Column(name="LU_METALS_TYPE_PURITY")
	private BigDecimal luMetalsTypePurity;

	@Column(name="METAL_PRICE")
	private BigDecimal metalPrice;

	@Column(name="MAKING_CHARGES")
	private BigDecimal makingCharges;

	/*@Column(name="metal_type")
	private String metalType;*/

	private int mix;

	@Column(name="MIX_DESC")
	private String mixDesc;

	@Column(name="NO_OF_UNITS_IN_STOCK")
	private int noOfUnitsInStock;

	@Column(name="NO_OF_VIEWS")
	private int noOfViews;

	private String photo1;

	private String photo2;

	private BigDecimal price;

	@Column(name="STONE_DIAMOND_PRICE")
	private BigDecimal stoneDiamondPrice;

	private BigDecimal tax;

	@Column(name="TOTAL_PRICE")
	private BigDecimal totalPrice;

	private BigDecimal weight;

	private BigDecimal width;
	
	@Column(name="METAL_OTHER_TYPE_DESC")
	private String metalOtherTypeDesc;
	
	@Column(name="ARTICLE_OTHER_TYPE_DESC")
	private String articleOtherTypeDesc;
	
	
	private String currency;
	@Column(name="show_price")
	private int showPrice;

	//bi-directional one-to-one association to ItemSource
	@MapsId
	@OneToOne
	@JoinColumn(name="item_id")
	@JsonBackReference
	private ItemSource itemSource;
	
	@ManyToOne
	@JoinColumn(name="LU_METALS_TYPE")
	private LuMetalsOffered luMetalsOffered;

	@ManyToOne
	@JoinColumn(name="LU_ARTICLES_TYPE")
	private LuArticlesOffered luArticlesOffered;


	public ItemDetail() {
	}

	public int getItemId() {
		return this.itemId;
	}

	public void setItemId(int itemId) {
		this.itemId = itemId;
	}

/*	public String getArticleType() {
		return this.articleType;
	}

	public void setArticleType(String articleType) {
		this.articleType = articleType;
	}
*/
	public String getAvailability() {
		return this.availability;
	}

	public void setAvailability(String availability) {
		this.availability = availability;
	}

	public int getAvailableSizes() {
		return this.availableSizes;
	}

	public void setAvailableSizes(int availableSizes) {
		this.availableSizes = availableSizes;
	}

	public String getBrand() {
		return this.brand;
	}

	public void setBrand(String brand) {
		this.brand = brand;
	}

	public int getCustomizable() {
		return this.customizable;
	}

	public void setCustomizable(int customizable) {
		this.customizable = customizable;
	}

    public Set<LuDeliveryOptions> getDeliveryOffered() {
        return deliveryOffered;
    }

    public void setDeliveryOffered(final Set<LuDeliveryOptions> deliveryOffered) {
        this.deliveryOffered = deliveryOffered;
    }

    public String getDescription() {
		return this.description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getDesign() {
		return this.design;
	}

	public void setDesign(String design) {
		this.design = design;
	}

	public String getDesign3() {
		return this.design3;
	}

	public void setDesign3(String design3) {
		this.design3 = design3;
	}

	public String getDimension() {
		return this.dimension;
	}

	public void setDimension(String dimension) {
		this.dimension = dimension;
	}

	public BigDecimal getDimondStoneCarats() {
		return this.dimondStoneCarats;
	}

	public void setDimondStoneCarats(BigDecimal dimondStoneCarats) {
		this.dimondStoneCarats = dimondStoneCarats;
	}

	public String getDimondStoneCertification() {
		return this.dimondStoneCertification;
	}

	public void setDimondStoneCertification(String dimondStoneCertification) {
		this.dimondStoneCertification = dimondStoneCertification;
	}

	public String getDimondStoneClarity() {
		return this.dimondStoneClarity;
	}

	public void setDimondStoneClarity(String dimondStoneClarity) {
		this.dimondStoneClarity = dimondStoneClarity;
	}

	public String getDimondStoneColour() {
		return this.dimondStoneColour;
	}

	public void setDimondStoneColour(String dimondStoneColour) {
		this.dimondStoneColour = dimondStoneColour;
	}

	public int getDimondStonePieces() {
		return this.dimondStonePieces;
	}

	public void setDimondStonePieces(int dimondStonePieces) {
		this.dimondStonePieces = dimondStonePieces;
	}

	public int getFacebookLikes() {
		return this.facebookLikes;
	}

	public void setFacebookLikes(int facebookLikes) {
		this.facebookLikes = facebookLikes;
	}

	public String getFacebookShareLink() {
		return this.facebookShareLink;
	}

	public void setFacebookShareLink(String facebookShareLink) {
		this.facebookShareLink = facebookShareLink;
	}

/*	public BigDecimal getFee() {
		return this.fee;
	}

	public void setFee(BigDecimal fee) {
		this.fee = fee;
	}*/

	public String getGender() {
		return this.gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public BigDecimal getHeight() {
		return this.height;
	}

	public void setHeight(BigDecimal height) {
		this.height = height;
	}

	public BigDecimal getLength() {
		return this.length;
	}

	public void setLength(BigDecimal length) {
		this.length = length;
	}

	/*public int getLuArticlesType() {
		return this.luArticlesType;
	}

	public void setLuArticlesType(int luArticlesType) {
		this.luArticlesType = luArticlesType;
	}*/

	public String getLuItemType() {
		return this.luItemType;
	}

	public void setLuItemType(String luItemType) {
		this.luItemType = luItemType;
	}
	/*
	public int getLuMetalsType() {
		return this.luMetalsType;
	}

	public void setLuMetalsType(int luMetalsType) {
		this.luMetalsType = luMetalsType;
	}
*/
	public String getLuMetalsTypeColour() {
		return this.luMetalsTypeColour;
	}

	public void setLuMetalsTypeColour(String luMetalsTypeColour) {
		this.luMetalsTypeColour = luMetalsTypeColour;
	}

	public String getLU_METALS_TYPE_GROSS_Weight() {
		return this.LU_METALS_TYPE_GROSS_Weight;
	}

	public void setLU_METALS_TYPE_GROSS_Weight(String LU_METALS_TYPE_GROSS_Weight) {
		this.LU_METALS_TYPE_GROSS_Weight = LU_METALS_TYPE_GROSS_Weight;
	}

	public String getLuMetalsTypeNetCertification() {
		return this.luMetalsTypeNetCertification;
	}

	public void setLuMetalsTypeNetCertification(String luMetalsTypeNetCertification) {
		this.luMetalsTypeNetCertification = luMetalsTypeNetCertification;
	}

	public BigDecimal getLU_METALS_TYPE_NET_Weight() {
		return this.LU_METALS_TYPE_NET_Weight;
	}

	public void setLU_METALS_TYPE_NET_Weight(BigDecimal LU_METALS_TYPE_NET_Weight) {
		this.LU_METALS_TYPE_NET_Weight = LU_METALS_TYPE_NET_Weight;
	}

	public BigDecimal getLuMetalsTypePurity() {
		return this.luMetalsTypePurity;
	}

	public void setLuMetalsTypePurity(BigDecimal luMetalsTypePurity) {
		this.luMetalsTypePurity = luMetalsTypePurity;
	}

	public BigDecimal getMetalPrice() {
		return this.metalPrice;
	}

	public void setMetalPrice(BigDecimal metalPrice) {
		this.metalPrice = metalPrice;
	}

	public BigDecimal getMakingCharges() {
		return this.makingCharges;
	}

	public void setMakingCharges(BigDecimal makingCharges) {
		this.makingCharges = makingCharges;
	}

	/*public String getMetalType() {
		return this.metalType;
	}

	public void setMetalType(String metalType) {
		this.metalType = metalType;
	}
*/
	public int getMix() {
		return this.mix;
	}

	public void setMix(int mix) {
		this.mix = mix;
	}

	public String getMixDesc() {
		return this.mixDesc;
	}

	public void setMixDesc(String mixDesc) {
		this.mixDesc = mixDesc;
	}

	public int getNoOfUnitsInStock() {
		return this.noOfUnitsInStock;
	}

	public void setNoOfUnitsInStock(int noOfUnitsInStock) {
		this.noOfUnitsInStock = noOfUnitsInStock;
	}

	public int getNoOfViews() {
		return this.noOfViews;
	}

	public void setNoOfViews(int noOfViews) {
		this.noOfViews = noOfViews;
	}

	public String getPhoto1() {
		return this.photo1;
	}

	public void setPhoto1(String photo1) {
		this.photo1 = photo1;
	}

	public String getPhoto2() {
		return this.photo2;
	}

	public void setPhoto2(String photo2) {
		this.photo2 = photo2;
	}

	public BigDecimal getPrice() {
		return this.price;
	}

	public void setPrice(BigDecimal price) {
		this.price = price;
	}

	public BigDecimal getStoneDiamondPrice() {
		return this.stoneDiamondPrice;
	}

	public void setStoneDiamondPrice(BigDecimal stoneDiamondPrice) {
		this.stoneDiamondPrice = stoneDiamondPrice;
	}

	public BigDecimal getTax() {
		return this.tax;
	}

	public void setTax(BigDecimal tax) {
		this.tax = tax;
	}

	public BigDecimal getTotalPrice() {
		return this.totalPrice;
	}

	public void setTotalPrice(BigDecimal totalPrice) {
		this.totalPrice = totalPrice;
	}

	public BigDecimal getWeight() {
		return this.weight;
	}

	public void setWeight(BigDecimal weight) {
		this.weight = weight;
	}

	public BigDecimal getWidth() {
		return this.width;
	}

	public void setWidth(BigDecimal width) {
		this.width = width;
	}

	public ItemSource getItemSource() {
		return this.itemSource;
	}

	public void setItemSource(ItemSource itemSource) {
		this.itemSource = itemSource;
	}

	public LuMetalsOffered getLuMetalsOffered() {
		return this.luMetalsOffered;
	}

	public void setLuMetalsOffered(LuMetalsOffered luMetalsOffered) {
		this.luMetalsOffered = luMetalsOffered;
	}

	public LuArticlesOffered getLuArticlesOffered() {
		return this.luArticlesOffered;
	}

	public void setLuArticlesOffered(LuArticlesOffered luArticlesOffered) {
		this.luArticlesOffered = luArticlesOffered;
	}

	public String getMetalOtherTypeDesc() {
		return metalOtherTypeDesc;
	}

	public void setMetalOtherTypeDesc(String metalOtherTypeDesc) {
		this.metalOtherTypeDesc = metalOtherTypeDesc;
	}

	public String getArticleOtherTypeDesc() {
		return articleOtherTypeDesc;
	}

	public void setArticleOtherTypeDesc(String articleOtherTypeDesc) {
		this.articleOtherTypeDesc = articleOtherTypeDesc;
	}

	public String getCurrency() {
		return currency;
	}

	public void setCurrency(String currency) {
		this.currency = currency;
	}

	public int getShowPrice() {
		return showPrice;
	}

	public void setShowPrice(int showPrice) {
		this.showPrice = showPrice;
	}
	
	
	
}