package com.sm.swarnakart.entities;

import java.io.Serializable;

import javax.persistence.*;


/**
 * The persistent class for the lu_guarantee_type_offered database table.
 * 
 */
@Entity
@Table(name="lu_guarantee_type_offered")
@NamedQuery(name=LuGuaranteeTypeOffered.LU_GUARANTEE_TYPE_OFFERED_FIND_ALL, query="SELECT l FROM LuGuaranteeTypeOffered l")
public class LuGuaranteeTypeOffered implements Serializable {
	public static final String LU_GUARANTEE_TYPE_OFFERED_FIND_ALL = "LuGuaranteeTypeOffered.findAll";

	private static final long serialVersionUID = 1L;

	@Id
	private int id;

	@Column(name="GUARANTEE_TYPE_OFFERED")
	private String guaranteeTypeOffered;

	public LuGuaranteeTypeOffered() {
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getGuaranteeTypeOffered() {
		return this.guaranteeTypeOffered;
	}

	public void setGuaranteeTypeOffered(String guaranteeTypeOffered) {
		this.guaranteeTypeOffered = guaranteeTypeOffered;
	}

}