package com.sm.swarnakart.entities;

import java.io.Serializable;
import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import org.codehaus.jackson.annotate.JsonBackReference;
import org.codehaus.jackson.annotate.JsonIgnore;
import org.codehaus.jackson.annotate.JsonManagedReference;


/**
 * The persistent class for the merchant database table.
 * 
 */
@Entity
@Table(name = "MERCHANT")
@NamedQueries({
	 @NamedQuery(
             name = Merchant.FIND_BY_MERCHANTH_OWNERNAME,
             query = "select m from Merchant m where ownerName = :ownerName"
     ),
	@NamedQuery(name="Merchant.findAll", query="SELECT m FROM Merchant m"),
	@NamedQuery(name=Merchant.FIND_RANDOM_RECORDS, query="SELECT merchant FROM Merchant merchant ORDER BY rand()")
	 
})
public class Merchant implements Serializable {
	private static final long serialVersionUID = 1L;
	
	public static final String FIND_BY_MERCHANTH_OWNERNAME = "merchant.findByOwnerName";

	public static final String FIND_RANDOM_RECORDS = "Merchant.findRandomRecords";
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private int id;

	@Column(name="create_date")
	private Timestamp createDate;

	@Column(name="display_shop_offerings_online")
	private String displayShopOfferingsOnline;

	@Column(name="head_branch_id")
	private int headBranchId;

	@Column(name="marketing_email")
	private String marketingEmail;

	@Column(name="marketing_person_name")
	private String marketingPersonName;

	@Column(name="marketing_person_phone_m1")
	private String marketingPersonPhoneM1;

	@Column(name="marketing_person_phone_m2")
	private String marketingPersonPhoneM2;

	/*@Column(name="membership_type")
	private String membershipType;*/

	@Column(name="merchant_manage_online")
	private String merchantManageOnline;

	/*@Column(name="merchant_type")
	private String merchantType;*/

	@Column(name="owner_name")
	private String ownerName;

	@Column(name="owner_phone_m")
	private String ownerPhoneM;

	@Column(name="quick_order_supported")
	private String quickOrderSupported;

	@Column(name="shop_address_ln1")
	private String shopAddressLn1;

	@Column(name="shop_address_ln2")
	private String shopAddressLn2;

	@Column(name="shop_area")
	private BigDecimal shopArea;

	@Column(name="shop_city")
	private String shopCity;

	@Column(name="shop_country")
	private String shopCountry;

	@Column(name="shop_fax")
	private String shopFax;

	@Column(name="shop_history_info")
	private String shopHistoryInfo;

	@Column(name="shop_id")
	private String shopId;

	@Column(name="shop_land_mark")
	private String shopLandMark;

	@Column(name="shop_logo_file_link")
	private String shopLogoFileLink;

	@Column(name="shop_logo_file_name")
	private String shopLogoFileName;


	@Column(name="shop_no_employes")
	private int shopNoEmployes;

	@Column(name="shop_phone_l")
	private String shopPhoneL;

	@Column(name="shop_phone_m")
	private String shopPhoneM;

	@Column(name="shop_region")
	private String shopRegion;

	@Column(name="shop_serving_since")
	private int shopServingSince;

	@Column(name="shop_state")
	private String shopState;

	@Column(name="shop_turnover")
	private int shopTurnover;

	/*@Column(name="shop_url")
	private String shopUrl;*/

	@Column(name="show_merchant_online")
	private String showMerchantOnline;

	@Column(name="update_date")
	private Timestamp updateDate;

	@Column(name="work_shop_area")
	private BigDecimal workShopArea;

	@Column(name="workshop_no_employes")
	private int workshopNoEmployes;
	
	@Column(name="branch_type")
	private String branchType;
	
	@Column(name="item_count")
	private int itemCount;

	
	//bi-directional many-to-one association to ItemSource
		@OneToMany(mappedBy="merchant", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
		//@Cascade(value={org.hibernate.annotations.CascadeType.ALL, org.hibernate.annotations.CascadeType.DELETE_ORPHAN})
		@JsonManagedReference
		private List<ItemSource> itemSources;
		
	//bi-directional one-to-one association to MerchantLogin
		@OneToOne
		@JoinColumn(name="merchant_login_id")
		@JsonBackReference
		private MerchantLogin merchantLogin;

		
		@ManyToOne
		@JoinColumn(name="membership_type")
		private LuMembershipType luMembershipType;
		
		@ManyToOne
		@JoinColumn(name="merchant_type")
		private LuMerchantType luMerchantType;
		
		public Merchant() {
		}
	
	public int getId() {
		return this.id;
	}

	public String getBranchType() {
		return branchType;
	}

	public void setBranchType(String branchType) {
		this.branchType = branchType;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Timestamp getCreateDate() {
		return this.createDate;
	}

	public void setCreateDate(Timestamp createDate) {
		this.createDate = createDate;
	}

	public String getDisplayShopOfferingsOnline() {
		return this.displayShopOfferingsOnline;
	}

	public void setDisplayShopOfferingsOnline(String displayShopOfferingsOnline) {
		this.displayShopOfferingsOnline = displayShopOfferingsOnline;
	}

	public int getHeadBranchId() {
		return this.headBranchId;
	}

	public void setHeadBranchId(int headBranchId) {
		this.headBranchId = headBranchId;
	}

	public String getMarketingEmail() {
		return this.marketingEmail;
	}

	public void setMarketingEmail(String marketingEmail) {
		this.marketingEmail = marketingEmail;
	}

	public String getMarketingPersonName() {
		return this.marketingPersonName;
	}

	public void setMarketingPersonName(String marketingPersonName) {
		this.marketingPersonName = marketingPersonName;
	}

	public String getMarketingPersonPhoneM1() {
		return this.marketingPersonPhoneM1;
	}

	public void setMarketingPersonPhoneM1(String marketingPersonPhoneM1) {
		this.marketingPersonPhoneM1 = marketingPersonPhoneM1;
	}

	public String getMarketingPersonPhoneM2() {
		return this.marketingPersonPhoneM2;
	}

	public void setMarketingPersonPhoneM2(String marketingPersonPhoneM2) {
		this.marketingPersonPhoneM2 = marketingPersonPhoneM2;
	}

	/*public String getMembershipType() {
		return this.membershipType;
	}

	public void setMembershipType(String membershipType) {
		this.membershipType = membershipType;
	}
*/
	public String getMerchantManageOnline() {
		return this.merchantManageOnline;
	}

	public void setMerchantManageOnline(String merchantManageOnline) {
		this.merchantManageOnline = merchantManageOnline;
	}

	
	public String getOwnerName() {
		return this.ownerName;
	}

	public void setOwnerName(String ownerName) {
		this.ownerName = ownerName;
	}

	public String getOwnerPhoneM() {
		return this.ownerPhoneM;
	}

	public void setOwnerPhoneM(String ownerPhoneM) {
		this.ownerPhoneM = ownerPhoneM;
	}

	public String getQuickOrderSupported() {
		return this.quickOrderSupported;
	}

	public void setQuickOrderSupported(String quickOrderSupported) {
		this.quickOrderSupported = quickOrderSupported;
	}

	public String getShopAddressLn1() {
		return this.shopAddressLn1;
	}

	public void setShopAddressLn1(String shopAddressLn1) {
		this.shopAddressLn1 = shopAddressLn1;
	}

	public String getShopAddressLn2() {
		return this.shopAddressLn2;
	}

	public void setShopAddressLn2(String shopAddressLn2) {
		this.shopAddressLn2 = shopAddressLn2;
	}

	public BigDecimal getShopArea() {
		return this.shopArea;
	}

	public void setShopArea(BigDecimal shopArea) {
		this.shopArea = shopArea;
	}

	public String getShopCity() {
		return this.shopCity;
	}

	public void setShopCity(String shopCity) {
		this.shopCity = shopCity;
	}

	public String getShopCountry() {
		return this.shopCountry;
	}

	public void setShopCountry(String shopCountry) {
		this.shopCountry = shopCountry;
	}

	public String getShopFax() {
		return this.shopFax;
	}

	public void setShopFax(String shopFax) {
		this.shopFax = shopFax;
	}

	public String getShopHistoryInfo() {
		return this.shopHistoryInfo;
	}

	public void setShopHistoryInfo(String shopHistoryInfo) {
		this.shopHistoryInfo = shopHistoryInfo;
	}

	public String getShopId() {
		return this.shopId;
	}

	public void setShopId(String shopId) {
		this.shopId = shopId;
	}

	public String getShopLandMark() {
		return this.shopLandMark;
	}

	public void setShopLandMark(String shopLandMark) {
		this.shopLandMark = shopLandMark;
	}

	public String getShopLogoFileLink() {
		return this.shopLogoFileLink;
	}

	public void setShopLogoFileLink(String shopLogoFileLink) {
		this.shopLogoFileLink = shopLogoFileLink;
	}

	public String getShopLogoFileName() {
		return this.shopLogoFileName;
	}

	public void setShopLogoFileName(String shopLogoFileName) {
		this.shopLogoFileName = shopLogoFileName;
	}


	public int getShopNoEmployes() {
		return this.shopNoEmployes;
	}

	public void setShopNoEmployes(int shopNoEmployes) {
		this.shopNoEmployes = shopNoEmployes;
	}

	public String getShopPhoneL() {
		return this.shopPhoneL;
	}

	public void setShopPhoneL(String shopPhoneL) {
		this.shopPhoneL = shopPhoneL;
	}

	public String getShopPhoneM() {
		return this.shopPhoneM;
	}

	public void setShopPhoneM(String shopPhoneM) {
		this.shopPhoneM = shopPhoneM;
	}

	public String getShopRegion() {
		return this.shopRegion;
	}

	public void setShopRegion(String shopRegion) {
		this.shopRegion = shopRegion;
	}

	public int getShopServingSince() {
		return this.shopServingSince;
	}

	public void setShopServingSince(int shopServingSince) {
		this.shopServingSince = shopServingSince;
	}

	public String getShopState() {
		return this.shopState;
	}

	public void setShopState(String shopState) {
		this.shopState = shopState;
	}

	public int getShopTurnover() {
		return this.shopTurnover;
	}

	public void setShopTurnover(int shopTurnover) {
		this.shopTurnover = shopTurnover;
	}

	/*public String getShopUrl() {
		return this.shopUrl;
	}

	public void setShopUrl(String shopUrl) {
		this.shopUrl = shopUrl;
	}
*/
	public String getShowMerchantOnline() {
		return this.showMerchantOnline;
	}

	public void setShowMerchantOnline(String showMerchantOnline) {
		this.showMerchantOnline = showMerchantOnline;
	}

	public Timestamp getUpdateDate() {
		return this.updateDate;
	}

	public void setUpdateDate(Timestamp updateDate) {
		this.updateDate = updateDate;
	}

	public BigDecimal getWorkShopArea() {
		return this.workShopArea;
	}

	public void setWorkShopArea(BigDecimal workShopArea) {
		this.workShopArea = workShopArea;
	}

	public int getWorkshopNoEmployes() {
		return this.workshopNoEmployes;
	}

	public void setWorkshopNoEmployes(int workshopNoEmployes) {
		this.workshopNoEmployes = workshopNoEmployes;
	}

	public MerchantLogin getMerchantLogin() {
		return this.merchantLogin;
	}

	public void setMerchantLogin(MerchantLogin merchantLogin) {
		this.merchantLogin = merchantLogin;
	}
	@JsonIgnore
	public List<ItemSource> getItemSources() {
		return this.itemSources;
	}

	public void setItemSources(List<ItemSource> itemSources) {
		this.itemSources = itemSources;
	}

	public ItemSource addItemSource(ItemSource itemSource) {
		getItemSources().add(itemSource);
		itemSource.setMerchant(this);

		return itemSource;
	}

	public ItemSource removeItemSource(ItemSource itemSource) {
		getItemSources().remove(itemSource);
		itemSource.setMerchant(null);

		return itemSource;
	}

	public LuMembershipType getLuMembershipType() {
		return luMembershipType;
	}

	public void setLuMembershipType(LuMembershipType luMembershipType) {
		this.luMembershipType = luMembershipType;
	}

	public LuMerchantType getLuMerchantType() {
		return luMerchantType;
	}

	public void setLuMerchantType(LuMerchantType luMerchantType) {
		this.luMerchantType = luMerchantType;
	}

	public int getItemCount() {
		return itemCount;
	}

	public void setItemCount(int itemCount) {
		this.itemCount = itemCount;
	}
	
	
	

}