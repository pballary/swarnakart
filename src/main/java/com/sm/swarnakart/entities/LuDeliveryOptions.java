package com.sm.swarnakart.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import java.io.Serializable;

/**
 * The persistent class for the lu_merchant_type database table.
 * 
 */
@Entity
@Table(name="lu_delivery_options")
@NamedQueries({
    @NamedQuery(name= LuDeliveryOptions.LU_DELIVERY_OPTIONS_FIND_ALL, query="SELECT l FROM LuDeliveryOptions l"),
    @NamedQuery(name= LuDeliveryOptions.LU_DELIVERY_OPTIONS_BY_KEY, query="SELECT l FROM LuDeliveryOptions  l where deliveryKey = :deliveryKey")
})
public class LuDeliveryOptions implements Serializable {
	private static final long serialVersionUID = 1L;

	public static final String LU_DELIVERY_OPTIONS_FIND_ALL = "LuDeliveryOptions.findAll";
	public static final String LU_DELIVERY_OPTIONS_BY_KEY = "LuDeliveryOptions.BYKEY";

	@Id
	private int id;

	@Column(name="delivery_key")
	private String deliveryKey;

	@Column(name="delivery_value")
	private String deliveryValue;


	public LuDeliveryOptions() {
	}

    public int getId() {
        return id;
    }

    public void setId(final int id) {
        this.id = id;
    }

    public String getDeliveryKey() {
        return deliveryKey;
    }

    public void setDeliveryKey(final String deliveryKey) {
        this.deliveryKey = deliveryKey;
    }

    public String getDeliveryValue() {
        return deliveryValue;
    }

    public void setDeliveryValue(final String deliveryValue) {
        this.deliveryValue = deliveryValue;
    }
}