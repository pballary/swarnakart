package com.sm.swarnakart.entities;

import java.io.Serializable;

import javax.persistence.*;

import org.codehaus.jackson.annotate.JsonBackReference;
import org.codehaus.jackson.annotate.JsonIgnore;
import org.codehaus.jackson.annotate.JsonManagedReference;

import java.util.Date;


/**
 * The persistent class for the item_source database table.
 * 
 */
@Entity
@Table(name="item_source")
@NamedQueries({
	@NamedQuery(name=ItemSource.ITEM_SOURCE_FIND_ALL, query="SELECT i FROM ItemSource i where row_count() <=50 order by updateDate desc"),
	@NamedQuery(name=ItemSource.ITEM_SOURCE_FIND_BY_MERCHANTH, query="SELECT i FROM ItemSource i where merchant =:merchant order by updateDate desc"),
	
})

public class ItemSource implements Serializable {
	public static final String ITEM_SOURCE_FIND_ALL = "ItemSource.findAll";


	public static final String ITEM_SOURCE_FIND_BY_MERCHANTH = "ItemSource.findByMerchant";


	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name="item_id")
	private int itemId;

	@Temporal(TemporalType.DATE)
	@Column(name="create_date")
	private Date createDate;

	@Temporal(TemporalType.DATE)
	@Column(name="expiery_date")
	private Date expieryDate;

	/*@Column(name="item_count")
	private int itemCount;*/

	@Column(name="search_priority")
	private String searchPriority;

	@Column(name="show_listing")
	private String showListing;

	@Temporal(TemporalType.DATE)
	@Column(name="update_date")
	private Date updateDate;
	
	@Transient
	private byte[] photo1Bytes;
	
	@Transient
	private byte[] photo2Bytes;

	//bi-directional one-to-one association to ItemDetail
	@OneToOne(mappedBy="itemSource", cascade = CascadeType.ALL,fetch = FetchType.LAZY)
	@JsonManagedReference
	private ItemDetail itemDetail;

	//bi-directional many-to-one association to Merchant
	/*@ManyToOne(cascade = CascadeType.ALL)
	@JoinColumn(name="merchant_id")
	private Merchant merchant;*/
	//bi-directional many-to-one association to Merchant
		@ManyToOne
		@JoinColumn(name="merchant_id")
		@JsonBackReference
		private Merchant merchant;
		

	public ItemSource() {
	}

	public int getItemId() {
		return this.itemId;
	}

	public void setItemId(int itemId) {
		this.itemId = itemId;
	}

	public Date getCreateDate() {
		return this.createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	public Date getExpieryDate() {
		return this.expieryDate;
	}

	public void setExpieryDate(Date expieryDate) {
		this.expieryDate = expieryDate;
	}

	/*public int getItemCount() {
		return this.itemCount;
	}

	public void setItemCount(int itemCount) {
		this.itemCount = itemCount;
	}*/

	public String getSearchPriority() {
		return this.searchPriority;
	}

	public void setSearchPriority(String searchPriority) {
		this.searchPriority = searchPriority;
	}

	public String getShowListing() {
		return this.showListing;
	}

	public void setShowListing(String showListing) {
		this.showListing = showListing;
	}

	public Date getUpdateDate() {
		return this.updateDate;
	}

	public void setUpdateDate(Date updateDate) {
		this.updateDate = updateDate;
	}

	public ItemDetail getItemDetail() {
		return this.itemDetail;
	}

	public void setItemDetail(ItemDetail itemDetail) {
		this.itemDetail = itemDetail;
	}
	@JsonIgnore
	public Merchant getMerchant() {
		return this.merchant;
	}

	public void setMerchant(Merchant merchant) {
		this.merchant = merchant;
	}

	public byte[] getPhoto1Bytes() {
		return photo1Bytes;
	}

	public void setPhoto1Bytes(byte[] photo1Bytes) {
		this.photo1Bytes = photo1Bytes;
	}

	public byte[] getPhoto2Bytes() {
		return photo2Bytes;
	}

	public void setPhoto2Bytes(byte[] photo2Bytes) {
		this.photo2Bytes = photo2Bytes;
	}

	

}