package com.sm.swarnakart.entities;

import java.io.Serializable;

import javax.persistence.*;


/**
 * The persistent class for the `lu_ articles _specialist` database table.
 * 
 */
@Entity
@Table(name="`lu_ articles _specialist`")
@NamedQuery(name=LuArticlesSpecialist.LU_ARTICLES_SPECIALIST_FIND_ALL, query="SELECT l FROM LuArticlesSpecialist l")
public class LuArticlesSpecialist implements Serializable {
	public static final String LU_ARTICLES_SPECIALIST_FIND_ALL = "LuArticlesSpecialist.findAll";

	private static final long serialVersionUID = 1L;

	@Id
	private int id;

	@Column(name="`ARTICLES _SPECIALIST`")
	private String articles_Specialist;

	public LuArticlesSpecialist() {
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getArticles_Specialist() {
		return this.articles_Specialist;
	}

	public void setArticles_Specialist(String articles_Specialist) {
		this.articles_Specialist = articles_Specialist;
	}

}