package com.sm.swarnakart.entities;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import org.codehaus.jackson.annotate.JsonManagedReference;


/**
 * The persistent class for the merchat_login database table.
 * 
 */
@Entity
@Table(name="merchant_login")
@NamedQueries( {@NamedQuery(name=MerchantLogin.FIND_ALL, query="SELECT m FROM MerchantLogin m"),
		        @NamedQuery(name=MerchantLogin.FIND_BY_USER_ID, query="select m from MerchantLogin m where userdId = :userdId"),
		        @NamedQuery(name=MerchantLogin.FIND_BY_SHOP_NAME, query="select m from MerchantLogin m where shopName = :shopName")

})
public class MerchantLogin implements Serializable {
	private static final long serialVersionUID = 1L;
	public static final String FIND_ALL = "MerchantLogin.findAll";
	public static final String FIND_BY_USER_ID = "MerchantLogin.findByUserId";
	public static final String FIND_BY_SHOP_NAME = "MerchantLogin.findByShopName";
	
	/*@GenericGenerator(name="loginIdGen" , strategy="increment")
	@GeneratedValue(generator="loginIdGen")*/
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name = "id")
	private int id;

	private String password;
	
	@Column(name = "temp_password")
	private String tempPassword;

	@Column(name="userd_id")
	private String userdId;
	
	@Column(name="shop_name")
	private String shopName;

	
	/*@Transient
	private boolean passwordChangeRequired = false;*/

	//bi-directional one-to-one association to Merchant
		@OneToOne(mappedBy="merchantLogin",fetch = FetchType.LAZY)
		@JsonManagedReference
		private Merchant merchant;

		public MerchantLogin() {
		}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getPassword() {
		return this.password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getUserdId() {
		return this.userdId;
	}

	public void setUserdId(String userdId) {
		this.userdId = userdId;
	}

	public Merchant getMerchant() {
		return this.merchant;
	}

	public void setMerchant(Merchant merchant) {
		this.merchant = merchant;
	}

	public String getTempPassword() {
		return tempPassword;
	}

	public void setTempPassword(String tempPassword) {
		this.tempPassword = tempPassword;
	}

	public String getShopName() {
		return this.shopName;
	}

	public void setShopName(String shopName) {
		this.shopName = shopName;
	}


}