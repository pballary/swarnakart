package com.sm.swarnakart.entities;

import java.io.Serializable;

import javax.persistence.*;


/**
 * The persistent class for the lu_metals_offered database table.
 * 
 */
@Entity
@Table(name="lu_metals_offered")
@NamedQuery(name=LuMetalsOffered.LU_METALS_OFFERED_FIND_ALL, query="SELECT l FROM LuMetalsOffered l")
public class LuMetalsOffered implements Serializable {
	public static final String LU_METALS_OFFERED_FIND_ALL = "LuMetalsOffered.findAll";

	private static final long serialVersionUID = 1L;

	@Id
	private int id;

	@Column(name="METALS_OFFERED")
	private String metalsOffered;

	public LuMetalsOffered() {
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getMetalsOffered() {
		return this.metalsOffered;
	}

	public void setMetalsOffered(String metalsOffered) {
		this.metalsOffered = metalsOffered;
	}

}