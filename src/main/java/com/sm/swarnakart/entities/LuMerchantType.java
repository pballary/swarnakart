package com.sm.swarnakart.entities;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.Table;


/**
 * The persistent class for the lu_merchant_type database table.
 * 
 */
@Entity
@Table(name="lu_merchant_type")
@NamedQuery(name=LuMerchantType.LU_MERCHANT_TYPES_FIND_ALL, query="SELECT l FROM LuMerchantType l")
public class LuMerchantType implements Serializable {
	private static final long serialVersionUID = 1L;
	
	public static final String LU_MERCHANT_TYPES_FIND_ALL = "LuMerchantType.findAll";

	@Id
	private int id;

	@Column(name="merchant_type")
	private String merchantType;

	
	public LuMerchantType() {
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getMerchantType() {
		return this.merchantType;
	}

	public void setMerchantType(String merchantType) {
		this.merchantType = merchantType;
	}


}