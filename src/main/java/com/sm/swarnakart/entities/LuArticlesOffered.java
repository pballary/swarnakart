package com.sm.swarnakart.entities;

import java.io.Serializable;

import javax.persistence.*;


/**
 * The persistent class for the lu_articles_offered database table.
 * 
 */
@Entity
@Table(name="lu_articles_offered")
@NamedQuery(name=LuArticlesOffered.LU_ARTICLES_OFFERED_FIND_ALL, query="SELECT l FROM LuArticlesOffered l")
public class LuArticlesOffered implements Serializable {
	public static final String LU_ARTICLES_OFFERED_FIND_ALL = "LuArticlesOffered.findAll";

	private static final long serialVersionUID = 1L;

	@Id
	private int id;

	@Column(name="ARTICLES_OFFERED")
	private String articlesOffered;

	public LuArticlesOffered() {
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getArticlesOffered() {
		return this.articlesOffered;
	}

	public void setArticlesOffered(String articlesOffered) {
		this.articlesOffered = articlesOffered;
	}

}