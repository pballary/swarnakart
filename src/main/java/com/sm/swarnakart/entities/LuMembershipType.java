package com.sm.swarnakart.entities;

import java.io.Serializable;

import javax.persistence.*;


/**
 * The persistent class for the lu_membership_type database table.
 * 
 */
@Entity
@Table(name="lu_membership_type")
@NamedQuery(name=LuMembershipType.LU_MEMBERSHIP_TYPE_FIND_ALL, query="SELECT l FROM LuMembershipType l")
public class LuMembershipType implements Serializable {
	public static final String LU_MEMBERSHIP_TYPE_FIND_ALL = "LuMembershipType.findAll";

	private static final long serialVersionUID = 1L;

	@Id
	private int id;

	@Column(name="MEMBERSHIP_TYPE")
	private String membershipType;
	
	@Transient
	private boolean disabled =false;

	public LuMembershipType() {
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getMembershipType() {
		return this.membershipType;
	}

	public void setMembershipType(String membershipType) {
		this.membershipType = membershipType;
	}

	public boolean isDisabled() {
		disabled = id==1?false:true;
		return disabled;
	}

	/*public void setDisabled(boolean disabled) {
		this.disabled = disabled;
	}*/

	

	/*public void setDisabled(boolean disabled) {
		this.isDisabled = disabled;
	}*/

	
}