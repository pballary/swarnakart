package com.sm.swarnakart.entities;

import java.io.Serializable;

import javax.persistence.*;


/**
 * The persistent class for the `lu_ metal_specialist` database table.
 * 
 */
@Entity
@Table(name="`lu_ metal_specialist`")
@NamedQuery(name=LuMetalSpecialist.LU_METAL_SPECIALIST_FIND_ALL, query="SELECT l FROM LuMetalSpecialist l")
public class LuMetalSpecialist implements Serializable {
	public static final String LU_METAL_SPECIALIST_FIND_ALL = "LuMetalSpecialist.findAll";

	private static final long serialVersionUID = 1L;

	@Id
	private int id;

	@Column(name="` METAL_SPECIALIST`")
	private String _metalSpecialist;

	public LuMetalSpecialist() {
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String get_metalSpecialist() {
		return this._metalSpecialist;
	}

	public void set_metalSpecialist(String _metalSpecialist) {
		this._metalSpecialist = _metalSpecialist;
	}

}