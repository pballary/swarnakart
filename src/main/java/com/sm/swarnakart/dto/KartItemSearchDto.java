package com.sm.swarnakart.dto;

import java.util.List;

public class KartItemSearchDto {
	private List<Integer> categories;
	private List<Integer> metalTypes;
	private String countryCode;
	private String stateCode;
	private String city;
	private String itemDescription;
	public List<Integer> getCategories() {
		return categories;
	}
	public void setCategories(List<Integer> categories) {
		this.categories = categories;
	}
	public List<Integer> getMetalTypes() {
		return metalTypes;
	}
	public void setMetalTypes(List<Integer> metalTypes) {
		this.metalTypes = metalTypes;
	}
	public String getCountryCode() {
		return countryCode;
	}
	public void setCountryCode(String countryCode) {
		this.countryCode = countryCode;
	}
	public String getStateCode() {
		return stateCode;
	}
	public void setStateCode(String stateCode) {
		this.stateCode = stateCode;
	}
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	public String getItemDescription() {
		return itemDescription;
	}
	public void setItemDescription(String itemDescription) {
		this.itemDescription = itemDescription;
	}
	
	

}
