package com.sm.swarnakart.dto;

import java.util.Set;

import com.sm.swarnakart.entities.ItemDetail;

public class kartItemDetailsDto {
	private String businessName;
	private byte[] merchantLogo;
	private ItemDetail itemDetail;
	private byte[] itemImage;
	private String MarketingName;
	private String marketingEmail;
	private String marketingPhone1;
	private String marketingPhone2;
	private String imageUUID;
	private Set<String> deliveryOffered;
	private String shopFullName;
	
	public String getBusinessName() {
		return businessName;
	}
	public void setBusinessName(String businessName) {
		this.businessName = businessName;
	}
	public byte[] getMerchantLogo() {
		return merchantLogo;
	}
	public void setMerchantLogo(byte[] merchantLogo) {
		this.merchantLogo = merchantLogo;
	}
	public ItemDetail getItemDetail() {
		return itemDetail;
	}
	public void setItemDetail(ItemDetail itemDetail) {
		this.itemDetail = itemDetail;
	}
	public byte[] getItemImage() {
		return itemImage;
	}
	public void setItemImage(byte[] itemImage) {
		this.itemImage = itemImage;
	}
	public String getMarketingName() {
		return MarketingName;
	}
	public void setMarketingName(String marketingName) {
		MarketingName = marketingName;
	}
	public String getMarketingEmail() {
		return marketingEmail;
	}
	public void setMarketingEmail(String marketingEmail) {
		this.marketingEmail = marketingEmail;
	}
	public String getMarketingPhone1() {
		return marketingPhone1;
	}
	public void setMarketingPhone1(String marketingPhone1) {
		this.marketingPhone1 = marketingPhone1;
	}
	public String getMarketingPhone2() {
		return marketingPhone2;
	}
	public void setMarketingPhone2(String marketingPhone2) {
		this.marketingPhone2 = marketingPhone2;
	}
	public String getImageUUID() {
		return imageUUID;
	}
	public void setImageUUID(String imageUUID) {
		this.imageUUID = imageUUID;
	}
	
	 public Set<String> getDeliveryOffered() {
	        return deliveryOffered;
	    }

	    public void setDeliveryOffered(final Set<String> deliveryOffered) {
	        this.deliveryOffered = deliveryOffered;
	    }
		public String getShopFullName() {
			return shopFullName;
		}
		public void setShopFullName(String shopFullName) {
			this.shopFullName = shopFullName;
		}
	
	

}
