package com.sm.swarnakart.dto;

import java.util.ArrayList;
import java.util.List;

public class MerchantItemDto {

	private String businessName;
	private String userId;
	private int merchantId;
	private int merchantLoginId;
	private byte[] merchantLogo;
	private String MarketingName;
	private String marketingEmail;
	private String marketingPhone1;
	private String marketingPhone2;
	private String addressLine1;
	private String addressLine2;
	private String country;
	private String State;
	private String city;
	private String logoURI;
	private List<ItemSummaryDto>  itemSummaryDtos = new ArrayList<ItemSummaryDto>();
	/**
	 * @return the businessName
	 */
	public String getBusinessName() {
		return businessName;
	}
	/**
	 * @param businessName the businessName to set
	 */
	public void setBusinessName(String businessName) {
		this.businessName = businessName;
	}
	/**
	 * @return the merchantId
	 */
	public int getMerchantId() {
		return merchantId;
	}
	/**
	 * @param merchantId the merchantId to set
	 */
	public void setMerchantId(int merchantId) {
		this.merchantId = merchantId;
	}
	/**
	 * @return the merchantLogo
	 */
	public byte[] getMerchantLogo() {
		return merchantLogo;
	}
	/**
	 * @param merchantLogo the merchantLogo to set
	 */
	public void setMerchantLogo(byte[] merchantLogo) {
		this.merchantLogo = merchantLogo;
	}
	/**
	 * @return the itemSummaryDtos
	 */
	public List<ItemSummaryDto> getItemSummaryDtos() {
		return itemSummaryDtos;
	}
	/**
	 * @param itemSummaryDtos the itemSummaryDtos to set
	 */
	public void setItemSummaryDtos(List<ItemSummaryDto> itemSummaryDtos) {
		this.itemSummaryDtos = itemSummaryDtos;
	}
	/**
	 * @return the userId
	 */
	public String getUserId() {
		return userId;
	}
	/**
	 * @param userId the userId to set
	 */
	public void setUserId(String userId) {
		this.userId = userId;
	}
	/**
	 * @return the merchantLoginId
	 */
	public int getMerchantLoginId() {
		return merchantLoginId;
	}
	/**
	 * @param merchantLoginId the merchantLoginId to set
	 */
	public void setMerchantLoginId(int merchantLoginId) {
		this.merchantLoginId = merchantLoginId;
	}
	public String getMarketingName() {
		return MarketingName;
	}
	public void setMarketingName(String marketingName) {
		MarketingName = marketingName;
	}
	public String getMarketingEmail() {
		return marketingEmail;
	}
	public void setMarketingEmail(String marketingEmail) {
		this.marketingEmail = marketingEmail;
	}
	public String getMarketingPhone1() {
		return marketingPhone1;
	}
	public void setMarketingPhone1(String marketingPhone1) {
		this.marketingPhone1 = marketingPhone1;
	}
	public String getMarketingPhone2() {
		return marketingPhone2;
	}
	public void setMarketingPhone2(String marketingPhone2) {
		this.marketingPhone2 = marketingPhone2;
	}
	public String getAddressLine1() {
		return addressLine1;
	}
	public void setAddressLine1(String addressLine1) {
		this.addressLine1 = addressLine1;
	}
	public String getAddressLine2() {
		return addressLine2;
	}
	public void setAddressLine2(String addressLine2) {
		this.addressLine2 = addressLine2;
	}
	public String getCountry() {
		return country;
	}
	public void setCountry(String country) {
		this.country = country;
	}
	public String getState() {
		return State;
	}
	public void setState(String state) {
		State = state;
	}
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	public String getLogoURI() {
		return logoURI;
	}
	public void setLogoURI(String logoURI) {
		this.logoURI = logoURI;
	}
	
	
	
}
