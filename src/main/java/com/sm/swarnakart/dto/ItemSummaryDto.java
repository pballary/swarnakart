package com.sm.swarnakart.dto;

import java.math.BigDecimal;
import java.util.Date;

import com.sm.swarnakart.entities.ItemDetail;

public class ItemSummaryDto {
	
	private String metalType;
	private String articleType;
	private Date updateDate;
	private byte[] photo1;
	private String currency;
	
	private ItemDetail itemDetail;
	
	private BigDecimal totalPrice;
	
	private byte[] photo2;
	
	private int srcId;
	private int itemId;
	
	private String photo1UUID;
	private String photo2UUID;
	public String getMetalType() {
		return metalType;
	}
	public void setMetalType(String metalType) {
		this.metalType = metalType;
	}
	public String getArticleType() {
		return articleType;
	}
	public void setArticleType(String articleType) {
		this.articleType = articleType;
	}
	public Date getUpdateDate() {
		return updateDate;
	}
	public void setUpdateDate(Date updateDate) {
		this.updateDate = updateDate;
	}
	public byte[] getPhoto1() {
		return photo1;
	}
	public void setPhoto1(byte[] photo1) {
		this.photo1 = photo1;
	}
	public int getSrcId() {
		return srcId;
	}
	public void setSrcId(int srcId) {
		this.srcId = srcId;
	}
	public int getItemId() {
		return itemId;
	}
	public void setItemId(int itemId) {
		this.itemId = itemId;
	}
	public BigDecimal getTotalPrice() {
		return totalPrice;
	}
	public void setTotalPrice(BigDecimal totalPrice) {
		this.totalPrice = totalPrice;
	}
	public String getCurrency() {
		return currency;
	}
	public void setCurrency(String currency) {
		this.currency = currency;
	}
	/**
	 * @return the itemDetail
	 */
	public ItemDetail getItemDetail() {
		return itemDetail;
	}
	/**
	 * @param itemDetail the itemDetail to set
	 */
	public void setItemDetail(ItemDetail itemDetail) {
		this.itemDetail = itemDetail;
	}
	/**
	 * @return the photo2
	 */
	public byte[] getPhoto2() {
		return photo2;
	}
	/**
	 * @param photo2 the photo2 to set
	 */
	public void setPhoto2(byte[] photo2) {
		this.photo2 = photo2;
	}
	public String getPhoto1UUID() {
		return photo1UUID;
	}
	public void setPhoto1UUID(String photo1uuid) {
		photo1UUID = photo1uuid;
	}
	public String getPhoto2UUID() {
		return photo2UUID;
	}
	public void setPhoto2UUID(String photo2uuid) {
		photo2UUID = photo2uuid;
	}
	
	
	
	

}
