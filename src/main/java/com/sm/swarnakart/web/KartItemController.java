package com.sm.swarnakart.web;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.sm.swarnakart.dto.ItemSummaryDto;
import com.sm.swarnakart.dto.KartItemSearchDto;
import com.sm.swarnakart.dto.MerchantItemDto;
import com.sm.swarnakart.dto.kartItemDetailsDto;
import com.sm.swarnakart.entities.ItemDetail;
import com.sm.swarnakart.entities.ItemSource;
import com.sm.swarnakart.exception.ErrorMessage;
import com.sm.swarnakart.exception.ValidationException;
import com.sm.swarnakart.services.KartItemService;

@RestController
@RequestMapping(value="/api/items/", produces=MediaType.APPLICATION_JSON_VALUE)
public class KartItemController {

	private static Logger logger = LoggerFactory.getLogger(KartItemController.class);
	
	@Autowired
	private KartItemService itemService;
	
	
	@ResponseStatus(HttpStatus.OK)
	@RequestMapping(value = "findAllItems", method = RequestMethod.GET)
	public List<ItemSummaryDto> getAllItems(){
		logger.info("Entering getAllItems()");
		List<ItemSummaryDto> itemSummary = new ArrayList<ItemSummaryDto>();
		itemSummary = itemService.findAllItems();
		logger.info("Exiting getAllItems()");
		return itemSummary;
	}
	
	
	@ResponseStatus(HttpStatus.OK)
	@RequestMapping(value = "itemsByBusinessName", method = RequestMethod.GET)
	public MerchantItemDto getitemListByBusinessName(@RequestParam(value="businessName", required=true) String businessName){
		logger.info("Entering getitemListByBusinessName()");
		MerchantItemDto merchantItems = new MerchantItemDto();
		merchantItems = itemService.findItemsbyBusinessName(businessName);
		logger.info("Exiting getitemListByBusinessName()");
		return merchantItems;
	}
	
	@Deprecated
	@ResponseStatus(HttpStatus.OK)
	@RequestMapping(value = "getDocumentByUUID", method = RequestMethod.GET)
	public ItemSummaryDto getDocumentByUUID(@RequestParam(value="uuid", required=true) String uuid){
		logger.info("Entering getDocumentByUUID::"+uuid);
		ItemSummaryDto itemSummaryDto = new ItemSummaryDto();
		itemSummaryDto.setPhoto2(itemService.getDocumentFile(uuid));
		logger.info("Exiting getDocumentByUUID::"+uuid);
		return itemSummaryDto;
	}
	
	@ResponseStatus(HttpStatus.OK)
	@RequestMapping(value = "getItemDetailById", method = RequestMethod.GET)
	public kartItemDetailsDto getItemDetailById(@RequestParam(value="sourceId", required=true) int sourceId){
		logger.info("Entering getItemDetailById::"+sourceId);
		kartItemDetailsDto kartItemDetailsDto = new kartItemDetailsDto();
		kartItemDetailsDto = itemService.getItemDetailsById(sourceId);
		logger.info("Exiting getItemDetailById::"+sourceId);
		return kartItemDetailsDto;
	}
	
	/*@ResponseStatus(HttpStatus.OK)
	@RequestMapping(value = "itemBySourceId", method = RequestMethod.GET)
	public ItemSource getItemBySourceId(@RequestParam(value="sourceId", required=true) int sourceId){
		logger.info("################# Inside MerchantController.getItemBySourceId::"+sourceId);
		ItemSource itemSource = null;
		itemSource = itemService.findItemBySourceId(sourceId);
		return itemSource;
	}
	*/
	@ResponseStatus(HttpStatus.OK)
	@RequestMapping(value = "getItemsByCriteria", method = RequestMethod.GET)
	public List<ItemSummaryDto> findItemsByCriteria(@RequestParam(value="itemSearch", required=true) String itemSearchDtoStr){
		logger.info("Entering findItemsByCriteria()");
		KartItemSearchDto itemSearchDto = new KartItemSearchDto();
		List<ItemSummaryDto> itemSummary = new ArrayList<ItemSummaryDto>();
		try{
		ObjectMapper mapper = new ObjectMapper();
		itemSearchDto = mapper.readValue(itemSearchDtoStr,KartItemSearchDto.class);
		itemSummary = itemService.findItemsByCriteria(itemSearchDto);
		}catch (JsonParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (JsonMappingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		logger.info("Exiting findItemsByCriteria()");
		return itemSummary;
	}
	@ExceptionHandler(Exception.class)
	public ResponseEntity<ErrorMessage> errorHandler(Exception exc) {
		ErrorMessage errorMsg = new ErrorMessage();
		errorMsg.setErrorCode(1);
		errorMsg.setErrorMsg(exc.getMessage());
		if(exc instanceof ValidationException){
			logger.error(exc.getMessage());
			return new ResponseEntity<>(errorMsg, HttpStatus.BAD_REQUEST);
		}else{
			logger.error(exc.getMessage(), exc);
			return new ResponseEntity<>(errorMsg, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

}
