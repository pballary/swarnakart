package com.sm.swarnakart.web;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.sm.swarnakart.dto.MerchantItemDto;
import com.sm.swarnakart.exception.ErrorMessage;
import com.sm.swarnakart.exception.ValidationException;
import com.sm.swarnakart.services.KartMerchantService;

@RestController
@RequestMapping(value="/api/merchants/", produces=MediaType.APPLICATION_JSON_VALUE)
public class KartMerchantController {

	private static Logger logger = LoggerFactory.getLogger(KartMerchantController.class);
	
	@Autowired
	private KartMerchantService merchantService;
	
	@Autowired
	private Environment env;
	
	
	@ResponseStatus(HttpStatus.OK)
	@RequestMapping(value = "getRandomMerchants", method = RequestMethod.GET)
	public List<MerchantItemDto> getRandomMerchants(){
		logger.info("Entering getRandomMerchants()");
		List<MerchantItemDto> merchants = new ArrayList<MerchantItemDto>();
		int maxRecordCount = env.getProperty("kart.merchant.maxCount") == null?10: Integer.valueOf(env.getProperty("kart.merchant.maxCount"));
		merchants = merchantService.findRandomMerchants(maxRecordCount);
		logger.info("Exiting getRandomMerchants()");
		return merchants;
	}
	
	
	@ExceptionHandler(Exception.class)
	public ResponseEntity<ErrorMessage> errorHandler(Exception exc) {
		ErrorMessage errorMsg = new ErrorMessage();
		errorMsg.setErrorCode(1);
		errorMsg.setErrorMsg(exc.getMessage());
		if(exc instanceof ValidationException){
			logger.error(exc.getMessage());
			return new ResponseEntity<>(errorMsg, HttpStatus.BAD_REQUEST);
		}else{
			logger.error(exc.getMessage(), exc);
			return new ResponseEntity<>(errorMsg, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

}
