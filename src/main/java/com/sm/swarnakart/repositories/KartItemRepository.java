package com.sm.swarnakart.repositories;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.apache.commons.lang3.StringUtils;
import org.hibernate.Criteria;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

import com.sm.swarnakart.dto.KartItemSearchDto;
import com.sm.swarnakart.entities.ItemDetail;
import com.sm.swarnakart.entities.ItemSource;
import com.sm.swarnakart.entities.Merchant;
import com.sm.swarnakart.entities.MerchantLogin;

@Repository
public class KartItemRepository {
	@PersistenceContext
	private EntityManager em;

	private static Logger logger = LoggerFactory.getLogger(KartItemRepository.class);
			
	public List<ItemSource> findItemsByMerchant(Merchant merchant){
		logger.info("Entering findItemsByMerchant");
		List<ItemSource> itemSourceList = new ArrayList<ItemSource>();
		itemSourceList = em.createNamedQuery(ItemSource.ITEM_SOURCE_FIND_BY_MERCHANTH,ItemSource.class)
						.setParameter("merchant", merchant)
						.getResultList();
		logger.info("Exiting findItemsByMerchant");
		return itemSourceList;
	}
	public MerchantLogin findByShopName(String businessName) throws NoResultException{
		logger.info("Entering findMerchantByBusinessName");
		MerchantLogin merchantLogin = em.createNamedQuery(MerchantLogin.FIND_BY_SHOP_NAME,MerchantLogin.class)
									  	.setParameter("shopName", businessName)
									  	.getSingleResult();
		logger.info("Exiting findMerchantByBusinessName");
		return merchantLogin;
	}
	public ItemSource findByItemId(int id){
		logger.info("Entering findByItemId");
		return em.find(ItemSource.class, id);
	}
	
	public List<ItemSource> findAllItems(){
		logger.info("Entering findAllItems");
		List<ItemSource> itemSourceList = new ArrayList<ItemSource>();
		itemSourceList = em.createNamedQuery(ItemSource.ITEM_SOURCE_FIND_ALL,ItemSource.class).getResultList();
		logger.info("exiting findAllItems");
		return itemSourceList;
	}
	
	public List<ItemSource> findItemsByCriteria(KartItemSearchDto itemSearchDto){
		logger.info("Entering findByExample");
		List<ItemSource> itemSourceList = new ArrayList<ItemSource>();
		/*List<Predicate> predicateList = new ArrayList<Predicate>();
		CriteriaBuilder builder = em.getCriteriaBuilder();
		CriteriaQuery<ItemSource> searchQuery = builder.createQuery(ItemSource.class);
		Root<ItemSource> itemSrc = searchQuery.from(ItemSource.class);
		searchQuery.select(itemSrc);
		if(itemSearchDto.getCategories() != null && itemSearchDto.getCategories().size()>0){
			searchQuery.where(itemSrc.get("").in(itemSearchDto.getCategories()));
		}
		if(itemSearchDto.getMetalTypes() != null && itemSearchDto.getMetalTypes().size()>0){
			searchQuery.where(itemSrc.get("").in(itemSearchDto.getMetalTypes()));
		}*/
		
		boolean isFirst = true; 
		StringBuilder query = new StringBuilder("from ItemSource ");
		if(itemSearchDto.getCategories() != null && itemSearchDto.getCategories().size()>0){
			if(isFirst){
				query.append(" where itemDetail.luArticlesOffered.id in :selectedCategories");
			}else{
				query.append(" and itemDetail.luArticlesOffered.id in :selectedCategories");
			}
			isFirst = false;
		}
		if(itemSearchDto.getMetalTypes() != null && itemSearchDto.getMetalTypes().size()>0){
			if(isFirst){
				query.append(" where itemDetail.luMetalsOffered.id in :selectedMetalTypes");
			}else{
				query.append(" and itemDetail.luMetalsOffered.id in :selectedMetalTypes");
			}
			isFirst = false;
		}
		
		if(!StringUtils.isBlank(itemSearchDto.getCountryCode())){
			if(isFirst){
				query.append(" where merchant.shopCountry = :selectedMerchantCountry");
			}else{
				query.append(" and merchant.shopCountry = :selectedMerchantCountry");
			}
			isFirst = false;
		}
		if(!StringUtils.isBlank(itemSearchDto.getStateCode())){
			if(isFirst){
				query.append(" where merchant.shopState = :selectedMerchantState");
			}else{
				query.append(" and merchant.shopState = :selectedMerchantState");
			}
			isFirst = false;
		}
		
		if(!StringUtils.isBlank(itemSearchDto.getCity())){
			if(isFirst){
				query.append(" where UPPER(merchant.shopCity) = :selectedMerchantCity");
			}else{
				query.append(" and UPPER(merchant.shopCity) = :selectedMerchantCity");
			}
			isFirst = false;
		}
		
		if(!StringUtils.isBlank(itemSearchDto.getItemDescription())){
			if(isFirst){
				query.append(" where UPPER(itemDetail.description) like :description");
			}else{
				query.append(" and UPPER(itemDetail.description) like :description");
			}
			isFirst = false;
		}
		
		
		TypedQuery<ItemSource> typedQuery = em.createQuery(query.toString(), ItemSource.class);
		if(itemSearchDto.getCategories() != null && itemSearchDto.getCategories().size()>0){
			typedQuery.setParameter("selectedCategories", itemSearchDto.getCategories());
		}
		if(itemSearchDto.getMetalTypes() != null && itemSearchDto.getMetalTypes().size()>0){
			typedQuery.setParameter("selectedMetalTypes", itemSearchDto.getMetalTypes());
		}
		if(itemSearchDto.getCountryCode() != null){
			typedQuery.setParameter("selectedMerchantCountry", itemSearchDto.getCountryCode());
		}
		if(itemSearchDto.getStateCode() != null){
			typedQuery.setParameter("selectedMerchantState", itemSearchDto.getStateCode());
		}
		if(!StringUtils.isBlank(itemSearchDto.getCity())){
			typedQuery.setParameter("selectedMerchantCity", itemSearchDto.getCity().toUpperCase());
		}
		if(!StringUtils.isBlank(itemSearchDto.getItemDescription())){
			typedQuery.setParameter("description", "%" +itemSearchDto.getItemDescription().toUpperCase()+ "%");
		}
		itemSourceList = typedQuery.getResultList();
		//itemSourceList = em.createNamedQuery(ItemSource.ITEM_SOURCE_FIND_ALL,ItemSource.class).getResultList();
		logger.info("exiting findByExample");
		return itemSourceList;
	}
}
