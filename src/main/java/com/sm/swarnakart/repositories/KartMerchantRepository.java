package com.sm.swarnakart.repositories;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

import com.sm.swarnakart.entities.Merchant;

@Repository
public class KartMerchantRepository {
	@PersistenceContext
	private EntityManager em;

	private static Logger logger = LoggerFactory.getLogger(KartMerchantRepository.class);
		public List<Merchant> getRandomMerchantList(int recordCount){
			logger.info("Entering getRandomMerchantList()");
			List<Merchant> merchants = new ArrayList<Merchant>();
			merchants = em.createNamedQuery(Merchant.FIND_RANDOM_RECORDS,Merchant.class).setMaxResults(recordCount).getResultList();
			logger.info("Exiting getRandomMerchantList()");
			return merchants;
		}

}
